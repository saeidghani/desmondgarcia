
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

const functions = require('firebase-functions')
const admin = require('firebase-admin')
const nodemailer = require('nodemailer')
const cors = require('cors')({ origin: true })
const firebase = require('firebase')
require('firebase/storage')

// Initializing Firebase Admin SDK
admin.initializeApp()

// Creating Nodemailer transporter using your Mailtrap SMTP details
const mailTransport = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  service: 'gmail',
  port: 587,
  auth: {
    user: 'noreply@desmondgarcia.com',
    pass: 'Test1Test2'
  }
})

// Creating a Firebase Cloud Function
exports.contactUsEmailSender = functions.https.onRequest((req, res) => {
  const mailOptions = {
    from: 'noreply@desmondgarcia.com',
    to: 'hey@desmondgarcia.com'
  }

  // in case you get CORS errors you need these 2 lines of code
  res.set('Access-Control-Allow-Origin', '*')
  res.set('Access-Control-Allow-Credentials', 'true')

  cors(req, res, () => {
    mailOptions.subject = 'Contact Form Message'
    mailOptions.html = `
          <p>Email: ${req.body.email}</p>
          <p>Name: ${req.body.name}</p>
          <p>Phone: ${req.body.phone}</p>
          <p>Message: ${req.body.message}</p>
          `
    return mailTransport.sendMail(mailOptions)
      .then(() => {
        return res.status(200).json({ success: true, Referer: req.header('Referer') })
      })
      .catch((e) => {
        return res.status(400).json({ err: e })
      })
  })
})

exports.careerFormEmailSender = functions.https.onRequest((req, res) => {
  res.set('Access-Control-Allow-Origin', '*')
  res.set('Access-Control-Allow-Credentials', 'true')
  const mailOptions = {
    from: 'noreply@desmondgarcia.com',
    to: 'nat@desmondgarcia.com'
  }
  cors(req, res, () => {
    mailOptions.subject = 'Career Form Message'
    mailOptions.html = `
          <p>fullName: ${req.body.fullName}</p>
          <p>email: ${req.body.email}</p>
          <p>phoneNumber: ${req.body.phoneNumber}</p>
          <p>address: ${req.body.address}</p>
          <p>Linkein: ${req.body.linkedin}</p>
          <p>Behance: ${req.body.behance}</p>
          <p>instagram: ${req.body.instagram}</p>
          <p>career: ${req.body.career}</p>
          `
    mailOptions.attachments = createAttachment(req)

    return mailTransport.sendMail(mailOptions)
      .then(() => {
        return res.status(200).json({ success: true })
      })
      .catch((e) => {
        return res.status(400).json({ err: e })
      })
  })
})

const createAttachment = (req) => {
  const attachments = []
  if (req.body.cvBase64 != null) {
    const obj1 = {
      filename: 'cv_' + req.body.cvFileName,
      content: req.body.cvBase64.split('base64,')[1],
      encoding: 'base64'
    }
    attachments.push(obj1)
  }

  if (req.body.portfolioBase64 != null) {
    const obj2 = {
      filename: 'portfolio_' + req.body.portfolioFileName,
      content: req.body.portfolioBase64.split('base64,')[1],
      encoding: 'base64'
    }
    attachments.push(obj2)
  }

  return attachments
}

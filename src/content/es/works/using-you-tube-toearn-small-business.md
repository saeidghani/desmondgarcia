---
name: Usando YouTube para atraer pequeños negocios
slug: using-you-tube-toearn-small-business
description: Usando YouTube para atraer pequeños negocios
thumbnail: /assets/img/projects/using-you-tube-toearn-small-business-cover.jpg
height: 720
width: 1280
categories: [Producción de video]
brand: DocuSign
home: true
sliderImages: ['/assets/img/projects/using-you-tube-toearn-small-business-slider-1.jpg']
video: 2sWqjaG_l5s
featureText: Video testimonios efectivos y poderosos
services: [Producción de video, Diseño, Estrategia]

description1: <p>desmond garcia se asoció con DocuSign para crear una serie de doce videos destinados para la web, YouTube, redes sociales y otros. Estos videos tenían diferentes funciones, dependiendo del canal de difusión, así que personalizamos iniciativas creativas y estratégicas para cada uno, incluyendo escalas 1:1 para móvil y gráficos personalizados. </p>

featureData: ['Los testimonios y las recomendaciones de pares son muy eficaces y poderosos.', 'El 97% de los clientes B2B <small>piensan que este es el tipo de contenido más confiable.</small>']

description2: <p>Esta serie de videos presentó a clientes reales, sus negocios y sus historias de aceleración comercial con DocuSign. desmond garcía dirigió todos los esfuerzos de producción y creativos y este video en específico, “EL ARTE DE LA COMIDA” con Clay Williams ha sido considerado uno de los mejores y más efectivos videos del año. Contáctenos para contarle como podemos hacerlo también por usted. </p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

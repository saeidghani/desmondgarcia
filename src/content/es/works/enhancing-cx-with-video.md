---
name: Mejorando la experiencia de usuario por medio de videos
slug: enhancing-cx-with-video
description: Mejorando la experiencia de usuario por medio de videos
thumbnail: /assets/img/projects/enhancing-cx-with-video-cover.jpg
height: 720
width: 1280
categories: [Producción de video]
brand: 23andMe
home: false
sliderImages: ['/assets/img/projects/enhancing-cx-with-video-slider-1.jpg']
video: m_BMytyLtOs
featureText: Vídeos de demostración para una mejor comprensión   
services: ['Producción de video', 'Diseño', 'Estrategia' ]

description1: <p>Conceptualizamos, produjimos, filmamos, editamos y finalizamos una serie de 6 videos diseñados para ayudarle a los clientes con su experiencia de usuario con el producto. Esto construyó afinidad con la marca y ayudó a convertir a los clientes nuevos en embajadores de la marca.</p>

featureData: []

description2: <p> Este es el video de demostración del producto 23andMe diseñado para ayudar a los nuevos clientes a comprender mejor su el recorrido por su ADN. </p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: 
---
# My first blog post

Welcome to my first blog post using content module

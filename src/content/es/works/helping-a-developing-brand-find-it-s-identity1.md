---
name: Ayudando a una marca en crecimiento a encontrar su identidad
slug: helping-a-developing-brand-find-it-s-identity1
description: Ayudando a una marca en crecimiento a encontrar su identidad1
thumbnail: /assets/img/projects/helping-a-developing-brand-find-it-s-identity1-cover.jpg
height: 720
width: 1280
categories: [Medición y Optimización]
brand: Castlight
home: false
sliderImages: ['/assets/img/projects/helping-a-developing-brand-find-it-s-identity1-cover.jpg','/assets/img/projects/helping-a-developing-brand-find-it-s-identity1-slider-1.jpg']
featureText: Involucrando a más y más clientes
services: [Medición y optimización, Diseño, Estrategia]

description1: <p>Castlight necesitaba una actualización de marca, nuevos folletos y materiales de marketing y ventas. Construimos y renovamos activos de mercadeo, reflejando la nueva marca, sus increíbles productos y sus excelentes capacidades, lo cual llevó a esta incipiente empresa a otro nivel./p>

featureData: []

description2: <p>Ayudamos a Castlight con análisis y comprensión de los hallazgos de Google y los próximos pasos a seguir para involucrar a sus clientes durante más tiempo y convertir a los visitantes de la etapa inicial en clientes.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: There's no photo asset for this as it's measurement & optimization
---
# My first blog post

Welcome to my first blog post using content module

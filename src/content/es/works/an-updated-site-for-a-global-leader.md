---
name: Un sitio renovado para un líder global
slug: an-updated-site-for-a-global-leader
description: Un sitio renovado para un líder global
thumbnail: /assets/img/projects/an-updated-site-for-a-global-leader-cover.jpg
height: 720
width: 1280
categories: [Interactivo/Web]
brand: Invisalign
home: false
sliderImages: ['/assets/img/projects/an-updated-site-for-a-global-leader-slider-1.jpg']
featureText: Mejorando hacia estándares globales
services: [Interactivo/Web, Medición y Optimización, Diseño]

description1: <p>desmond garcia ejecutó una experiencia de usuario global en cuanto al mensaje de comunicación para los mercados de Norte América, Europa y Asia lo cuál ayudó a la empresa a conectarse mejor con sus clientes.</p>

featureData: []

description2: <p>Además de este amplio esfuerzo estratégico, dg creó una aplicación para iPad que permite hablilitar ventas al equipo de ventas de Norte América y además permite ver contenido de video adicional.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use Invisalign Case Study
---
# My first blog post

Welcome to my first blog post using content module

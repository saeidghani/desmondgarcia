---
name: Entusiasmando los asistentes a conferencias
slug: firing-up-conference-audiences
description: Entusiasmando los asistentes a conferencias
thumbnail: /assets/img/projects/firing-up-conference-audiencess-cover.jpg
height: 720
width: 1280
categories: [Producción de video]
brand: DocuSign
home: false
sliderImages: ['/assets/img/projects/firing-up-conference-audiencess-slider-1.jpg']
video: 5hnnDk28N58
featureText: Un video de apertura para una conferencia importante
services: [Producción de video, Diseño, Estrategia]

description1: <p>Bien sea en el mundo físico, o ahora en uno más virtual, las conferencias son clave en muchas empresas, sean estas de Silicon Valley, nuevas o establecidas. En dg creamos muchos videos de conferencias para la reunión anual MOMENTUM de DocuSign.</p>

featureData: []

description2: <p>La intención era entusiasmar a la audiencia y construir una afinidad con la marca antes y durante el evento.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

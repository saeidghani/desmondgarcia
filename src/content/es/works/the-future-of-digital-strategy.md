---
name: El futuro de la estrategia digital
slug: the-future-of-digital-strategy
description: El futuro de la estrategia digital
thumbnail: /assets/img/projects/the-future-of-digital-strategy-cover.jpg
height: 800
width: 800
categories: [Branding]
brand: Wells Fargo
home: false
sliderImages: []
featureText:
services: [Estrategia, Branding, Diseño]

description1: <p>Debido a la sensible naturaleza del branding, no compartimos trabajo de marca públicamente. Esta es una lista de clientes con los que hemos colaborado para crear audiencias, brand personas, trabajo de marca, desarrollo y posicionamiento.</p>

featureData: []

description2:

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Not sure what to do here. Open to suggestions
---
# My first blog post

Welcome to my first blog post using content module

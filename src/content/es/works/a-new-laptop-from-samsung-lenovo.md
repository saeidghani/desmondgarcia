---
name: Un nuevo portátil de Samsung y Lenovo
slug: a-new-laptop-from-samsung-lenovo
description: Un nuevo portátil de Samsung y Lenovo
thumbnail: /assets/img/projects/a-new-laptop-from-samsung-lenovo-cover.jpg
height: 720
width: 1280
categories: [Producción de video]
brand: Samsung/Lenovo
home: false
sliderImages: ['/assets/img/projects/a-new-laptop-from-samsung-lenovo-slider-1.jpg']
video: LOBKLQioicE
featureText: Un video de alta calidad que cuenta una gran historia
services: [Producción de video, Diseño, Estrategia]

description1: <p>Cuando Lenovo estaba lanzando su nuevo portatil en colaboración con Samsung, nos buscaron para que les ayudáramos a contar una mejor historia, diseñar las animaciones 3D de la parte interna del producto y producir un video de alta calidad que ayudara a contar esta historia. </p>

featureData: []

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

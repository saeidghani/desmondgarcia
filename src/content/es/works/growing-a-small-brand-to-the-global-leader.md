---
name: Convirtiendo una pequeña marca en un líder global
slug: growing-a-small-brand-to-the-global-leader
description: Convirtiendo una pequeña marca en un líder global
thumbnail: /assets/img/projects/growing-a-small-brand-to-the-global-leader-cover.jpg
height: 800
width: 800
categories: [Estrategia]
brand: DocuSign
home: false
sliderImages: ['/assets/img/projects/growing-a-small-brand-to-the-global-leader-slider-1.jpg']
video: XEDT8JjvAqE 
featureText: Creación de todo tipo de contenido y activos de diseño
services: [Strategy, Branding, Design, Video Production, Measurement & Optimization, Interactive/Web]

description1: <h4>DESAFÍO</h4> <p>Hace 9 años comenzamos a trabajar con una pequeña empresa llamada DocuSign. Eran más conocidos por ayudar a las personas a firmar, enviar y administrar documentos en la nube de manera fácil y segura. Pero la compañía buscaba ir más allá de una simple aplicación de firma, a una oferta mucho más grande de Gestión de transacciones digitales, una plataforma que ayudará a las pequeñas y medianas empresas a mantener los procesos 100% digitales de principio a fin para acelerar las transacciones. reducir costos y deleitar a clientes, socios, proveedores y empleados. Lo que necesitaban era una forma de explicar su propuesta de valor básica y su crecimiento, y al mismo tiempo atraer nuevas ventas para empresas y usuarios individuales. </p>

featureData: ['$220M a $41.35 <small>Bvalor de la compañía</small>', '684% crecimiento <small>en nuevos usuarios</small>', 'ADOPCIÓN 100% <small>Fortune 100</small>']

description2: '<h4>IDEAS </h4> <br> desmond garcia trajo a DocuSign nuestro enfoque principal: marca global y estrategias de contenido, seguidas por diseño, producción de video, activos interactivos y creación de contenido. Desde que trabajamos con DocuSign, hemos creado grandes estrategias de contenido, estrategias de contenido específicas para campañas, videos tutoriales, videos de himnos, sizzle videos, videos de productos, micrositios y hemos apoyado a DocuSign con la creación de todo tipo de activos de contenido y diseño para sus principales conferencias, eventos y adquisiciones.</p> <br><br> <h4>RESULTADOS </h4> <br> Los resultados hablan por sí mismos:aumento del 684% en nuevos usuarios, adopción por parte de todas las empresas de la lista Fortune 100 y un aumento del valor de la empresa de $ 220 millones a $ 41,35 mil millones.</p>'

gallery: []
Source file: https://drive.google.com/file/d/1pbXhRUFsI60leHixu3G3Grd8FRvkYDcA/view 
Notes: <li>Use Castlight_Health_One_Sheet_RGB.pdf</li>
---
# My first blog post

Welcome to my first blog post using content module

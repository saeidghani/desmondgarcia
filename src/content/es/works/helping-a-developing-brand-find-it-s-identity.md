---
name: Ayudando a una marca en cecimiento a encontrar su identidad
slug: helping-a-developing-brand-find-it-s-identity
description: Ayudando a una marca en cecimiento a encontrar su identidad
thumbnail: /assets/img/projects/helping-a-developing-brand-find-it-s-identity-cover.jpg
height: 720
width: 1280
categories: [Diseño]
brand: Castlight
home: false
sliderImages: ['/assets/img/projects/helping-a-developing-brand-find-it-s-identity-slider-1.jpg']
featureText: Activos nuevos y mejorados
services: [Diseño, Estrategia, Branding]

description1: <p>Castlight necesitaba una actualización de marca, nuevos folletos y materiales de marketing y ventas. </p>

featureData: []

description2: <p>Construimos y renovamos activos de mercadeo, reflejando la nueva marca, sus increíbles productos y sus excelentes capacidades, lo cual llevó a esta incipiente empresa a otro nivel.</p>

gallery: []
Source file: https://drive.google.com/file/d/1pbXhRUFsI60leHixu3G3Grd8FRvkYDcA/view 
Notes: <li>Use Castlight_Health_One_Sheet_RGB.pdf</li>
---
# My first blog post

Welcome to my first blog post using content module

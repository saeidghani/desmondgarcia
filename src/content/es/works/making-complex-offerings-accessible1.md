---
name: Acercadndo las ofertas complejas
slug: making-complex-offerings-accessible1
description: Acercadndo las ofertas complejas
thumbnail: /assets/img/projects/making-complex-offerings-accessible1-cover.jpg
height: 800
width: 800
categories: [Medición y Optimización]
brand: Blackberry
home: false
sliderImages: []
featureText: Informes analíticos personalizados
services: [Medición y optimización, Diseño, Estrategia]

description1: <p>Blackberry (Good) genera datos de marketing para comprenderlos y convertirlos en insights accionables. Trabajamos con Blackberry para entender mejor las estadísticas y comprender los hallazgos y determinar los siguientes pasos para Good.</p>

featureData: []

description2: <p>Creamos aplicaciones e informes analíticos personalizables que permitieron a los equipos de ventas llegar con más datos y obtener una mejor conversión.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Blackberry Good_Workflow_Builder_5_6
---
# My first blog post

Welcome to my first blog post using content module

---
name: Una nueva imagen para una marca emergente
slug: a-new-look-for-an-up-and-coming-brand
description: Una nueva imagen para una marca emergente
thumbnail: /assets/img/projects/a-new-look-for-an-up-and-coming-brand-cover.jpg
height: 720
width: 1280
categories: [Interactivo/Web]
brand: Castlight
home: false
sliderImages: []
featureText: Un cambio extremo
services: [Interactivo/Web, Medición y Optimización, Diseño]

description1: <p>Castlight necesitaba refrescar la marca, desde el logotipo hasta la voz, así que nosotros creamos y actualizamos las experiencias interactivas que reflejaban una nueva renovación e intención de marca. El resultado:un cliente super feliz y un aumento significativo en sus ingresos.</p>

featureData: []

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use Castlight Castlight_Ebook_07.29.14 or Castlight_Ebook_Presentation02
---
# My first blog post

Welcome to my first blog post using content module

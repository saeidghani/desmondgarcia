---
name: Inspirando a los asisentes a conferencias y afianzando alianzas
slug: inspiring-conference-audiences-partnerships
description: Inspirando a los asisentes a conferencias y afianzando alianzas
thumbnail: /assets/img/projects/inspiring-conference-audiences-partnerships-cover.jpg
height: 800
width: 800
categories: [Producción de video]
brand: TED
home: false
sliderImages: ['/assets/img/projects/inspiring-conference-audiences-partnerships-slider-1.jpg']
video: tPAf1xC2odI
featureText: Comunicando el poder femenino
services: [Producción de video, Diseño, Estrategia]

description1: <p>desmond garcia se asoció con TED para grabar una serie de videos para TEDWomen (TED Mujeres) y discutir el importante papel de la diversidad en los negocios y el poder de la mentoría femenina con altos ejecutivos de todo el país.</p>

featureData: []

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

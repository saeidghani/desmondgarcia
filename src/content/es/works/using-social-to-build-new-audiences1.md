---
name: Uso de las redes sociales para construir nuevas audiencias
slug: using-social-to-build-new-audiences1
description: Uso de las redes sociales para construir nuevas audiencias1
thumbnail: /assets/img/projects/using-social-to-build-new-audiences1-cover.jpg
height: 720
width: 1280
categories: [Producción de video]
brand: EJ Gallo
home: false
sliderImages: ['/assets/img/projects/using-social-to-build-new-audiences1-slider-1.jpg']
video: jfDlxzKwrUg
featureText: Servicio 360º de la agencia
services: [Producción de video, Diseño, Estrategia]

description1: <p>desmond garcia conoce a los Millennials y la Generación Y, ambos objetivos actuales y futuros. Nosotros recomendamos que en el punto cero de la verdad, el sitio "muestre y cuente".  Nuestra audiencia objetivo ama las experiencias auténticas, las redes sociales y desea conocer sobre las nuevas ofertas de productos y las diferentes maneras de utilizar los mismos. Entonces la experiencia .com fue diseñada para ser limpia, presentda primero en la tecnología móvil y con contenido que la audiencia quisiera "pinear", "postear", tweetear y compartir con el mundo. También creamos una experiencia de marca auténtica por medio de la construcción de un micrositio donde los usuarios pueden "postear" su propio contenido y creamos calendarios de redes sociales y posts que se vinculan sin mayor costo, a través de los canales de las redes sociales tradicionales.</p>

featureData: []

description2: <p>Después de muchos meses de diseño, desarrollo y activación - tráfico, enganche- lo más importante, las ventas se triplicaron. Gallo creó un SKU adicional para la marca, dando como resultado aun mayores ventas y contenido adicional. Este fue un esfuerzo de toda la agencia, desde la estrategia, marca, diseño, producción de video y fotografía, manejo de redes sociales y otras tareas.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

---
name: Un renacimiento total de identidad y marca
slug: a-full-identity-brand-rebirth
description: Un renacimiento total de identidad y marca
thumbnail: /assets/img/projects/a-full-identity-brand-rebirth-cover.jpg
height: 800
width: 800
categories: [Branding]
brand: EJ Gallo
home: false
sliderImages: []
featureText:
services: [Estrategia, Branding, Diseño]

description1: <p>Debido a la sensible naturaleza del branding, no compartimos trabajo de marca públicamente. Esta es una lista de clientes con los que hemos colaborado para crear audiencias, brand personas, trabajo de marca, desarrollo y posicionamiento.</p>

featureData: []

description2:

gallery: []
Source file: https://drive.google.com/file/d/0B106BJcGmDdsZ1N6VWNuc0FGdkk/view?usp=sharing
Notes: Not sure what to do here. Open to suggestions
---
# My first blog post

Welcome to my first blog post using content module

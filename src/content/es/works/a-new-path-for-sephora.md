---
name: Abriendo un nuevo camino para Sephora
slug: a-new-path-for-sephora
description: Abriendo un nuevo camino para Sephora
thumbnail: /assets/img/projects/a-new-path-for-sephora-cover.jpg
height: 800
width: 800
categories: [Estrategia]
brand: Sephora
home: true
sliderImages: ['/assets/img/projects/a-new-path-for-sephora-slider-1.jpg']
featureText: Un análisis profundo del ecosistema digital de Sephora
services: [Estrategia, Branding, Diseño]

description1: <h4>DESAFÍO</h4> <p> Sephora contrató a desmond garcia para realizar una auditoría digital completa que proporcionaría información técnica para líderes de marketing, estrategas de contenido y  líderes de contenido y sitio de Sephora para tomar decisiones tanto de mejoras como "lo que sigue" para Sephora. La solicitud era completar esto de manera imparcial, en gran parte sin un contexto de operaciones comerciales y de ventas. Sephora tiene una gran cantidad de marcas tanto en sus tiendas como en línea. Contenido, marca, mensajes y "¿qué tenemos?" puede volverse complicado y olvidado en un entorno tan acelerado.</p>

featureData: ['4% INMEDIATO <small>crecimiento ingresos trimestrales</small>', 'DOBLE DÍGITO <small>crecimiento interanual</small>', 'DEFINIMOS A SEPHORA <small>como “disruptor digital”</small>']

description2: <h4>IDEAS</h4> <p>desmond garcia hizo un análisis profundo del ecosistema digital de Sephora, proporcionando información cuantitativa sobre "dónde está todo y cómo se está desempeñando" basado en todos los tipos de categorías de contenido en todas las plataformas digitales, desde web, redes sociales, móvil y aplicaciones especializadas. A continuación, hicimos sugerencias específicas no solo para cada canal, sino también para cada medio / plataforma. Estas recomendaciones se basaron en las tendencias existentes de los consumidores y las tendencias futuras, como la participación a través de videos, micrositios, y plataformas que pueden haber sido subutilizadas pero ciertamente fueron efectivas.</p> <br><br> <h4>RESULTADOS</h4> <p>Sephora vió un aumento trimestral del 4% en los ingresos. Aprovechar nuestros descubrimientos y recomendaciones también resultó en un impulso exponencial en el tráfico a través del sitio, más tiempo dedicado por usuario en el sitio, plataformas, interacción móvil mayor y más profunda, y una historia de marca más completa y consistente a través de todos los canales.</p>

gallery: [/assets/img/projects/a-new-path-for-sephora-gallery-1.jpg, /assets/img/projects/a-new-path-for-sephora-gallery-2.jpg]

Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Sephora Case Study In Deck
---
# My first blog post

Welcome to my first blog post using content module

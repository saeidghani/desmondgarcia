---
name: Mejorando la marca en múltiples puntos de contacto
slug: improving-the-brand-on-mutliple-touchpoints
description: Mejorando la marca en múltiples puntos de contacto
thumbnail: /assets/img/projects/improving-the-brand-on-mutliple-touchpoints-cover.jpg
height: 800
width: 800
categories: [Diseño]
brand: DocuSign
home: true
sliderImages: ['/assets/img/projects/improving-the-brand-on-mutliple-touchpoints-slider-1.jpg']
video: JFKJ5lm-1t0
featureText: Creando todo tipo de contenido
services: [Diseño, Estrategia, Branding]

description1: '<p>En desmond garcia llevamos a DocuSign nuestro enfoque principal: estrategias generales de marca y contenido, seguido de diseño, producción de video, activos interactivos y creación de contenido. Desde que comenzamos a trabajar con DocuSign, hemos creado amplias estrategias de contenido, estrategias, campañas, videos tutoriales, videos de marca, videos cortos, videos de producto, mensajes de comunicación, micrositios y además hemos apoyado a DocuSign con la creación de todo tipo de contenido y activos de diseño para sus principales conferencias, eventos y adquisiciones.'
 
featureData: []

description2: <p>Los resultados hablan por sí mismos:un aumento del 684% en nuevos usuarios, adopción por parte de todas las empresas de la lista Fortune 100 y un aumento del valor de la empresa de $ 220 millones a $ 41,35 billones. </p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use something you've designed...
---

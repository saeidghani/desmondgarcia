---
name: Lanzando una nueva marca
slug: launching-a-new-brand
description: Lanzando una nueva marca
thumbnail: /assets/img/projects/launching-a-new-brand-cover.jpg
height: 720
width: 1280
categories: [Estrategia]
brand: EJ Gallo
home: true
order: 3
sliderImages: ['/assets/img/projects/launching-a-new-brand-slider-1.jpg']
video: hgFc6SrZjVA
featureText: Un nuevo sitio y experiencias de marca más auténticas
services: [Estrategia, Branding, Diseño, Producción de video, Medición y Optimización, Interactivo/Web]

description1: <h4>DESAFÍO</h4> <br /> <p>EJ Gallo lanzó un nuevo y emocionante licor (Viniq), pero las ventas los dos primeros años estuvieron estancadas. A los clientes les encantó, pero el conocimiento de la marca era bajo (3%) y la tasa de recompra del consumidor estaba por debajo de la categoría. Viniq necesitaba fortalecer la experiencia de marca. El sitio web y el contenido debían responder a las preguntas de quién, qué y cómo sobre Viniq y apoyar el reconocimiento a través de las redes sociales y mejorar el engagement, las frecuencia de visitas al sitio y la afinidad de marca.</p>

featureData: ['10X <small>Tráfico a la web</small>', '3X SALES <small>crecimiento interanual</small>', '1375% MAYOR <small>Interacción online</small>']

description2: <h4>IDEAS</h4> <p>desmond garcia, basado en su profundo conocimiento del público objetivo actual y futuro -Millennials y Gen Y,. Ellos aman las experiencias auténticas, las redes sociales y desean conocer de inmediato las ofertas de nuevos productos y las diferentes formas de usarlos. Por eso, la experiencia .com fue diseñada para ser limpia, móvil-primero y con contenido que este público quiera guardar, twittear, publicar y compartir con el mundo. También creamos experiencias de marca auténticas mediante la creación de un micrositio integrado donde los usuarios podían publicar su propio contenido generado por ellos mismos, y creamos calendarios de redes sociales y publicaciones que se vinculaban a esto sin grandes gastos a través de los canales de redes sociales tradicionales.</p> <h4>RESULTADOS</h4> <p>Después de varios meses de diseño, desarrollo y activación, el tráfico, la participación y, lo que es más importante, las ventas se triplicaron. Gallo creó un SKU adicional para la marca, lo que resultó en mayores ventas y contenido adicional. Este fue un esfuerzo con un alcance muy completo, desde la estrategia, la marca, el diseño, la producción de video y fotografía, la gestión de redes sociales y otras tareas.</p>

gallery: []
Source file: 
Notes: 
---
# My first blog post

Welcome to my first blog post using content module

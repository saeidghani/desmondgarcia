---
name: Combinando Google y DocuSign
slug: combining-google-and-docusign
description: Combinando Google y DocuSign
thumbnail: /assets/img/projects/combining-google-and-docusign-cover.jpg
height: 800
width: 800
categories: [Producción de video]
brand: DocuSign
home: false
sliderImages: ['/assets/img/projects/combining-google-and-docusign-slider-1.jpg']
video: qGuYT8n9yg8
featureText: Agregando valor a través del storytelling
services: [Producción de video, Diseño, Estrategia]

description1: <p>Cuando Google se asoció con DocuSign, nos buscaron para ayudar a contar la historia de la unión de sus productos.</p>

featureData: []

description2: <p>Nuestro objetivo fue tomar algo digital y potencialmente plano y agregarle una buena historia que resonara con todo tipo de clientes.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

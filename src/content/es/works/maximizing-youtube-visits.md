---
name: Maximizando las visitas a YouTube
slug: maximizing-youtube-visits
description: Maximizando las visitas a YouTube
thumbnail: /assets/img/projects/maximizing-youtube-visits-cover.jpg
height: 720
width: 1280
categories: [Medición y Optimización]
brand: DocuSign 
home: false
sliderImages: ['/assets/img/projects/maximizing-youtube-visits-slider-1.jpg']
video: B3K1qJaEcJk
featureText: Millones de visitas, sí, millones
services: [Medición y optimización, Diseño, Estrategia]

description1: <p>¿Quieres saber cómo hacer los mejores videos para pre-roll de YouTube? ¡Trabaja con nosotros! Trabajamos con DocuSign para crear docenas de videos producidos, diseñados y editados para pre-roll de YouTube.</p>

featureData: []

description2: <p>El resultado fue millones, sí, millones de conversiones y fortalecimiento de la marca DocuSign.  </p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: There's no photo asset for this as it's measurement & optimization
---
# My first blog post

Welcome to my first blog post using content module

---
name: Un micrositio para clientes donde puedan construir su propia experiencia
slug: microsite-for-customers-to-build-their-own-experience
description: Un micrositio para clientes donde puedan construir su propia experiencia
thumbnail: /assets/img/projects/microsite-for-customers-to-build-their-own-experience-cover.jpg
width: 800
height: 800
categories: [Interactivo/Web]
brand: Blackberry 
home: false
sliderImages: []
featureText: Experiencia de cliente personalizada
services: [Interactivo/Web, Medición y Optimización, Diseño]

description1: <p>Cuando Blackberry (Good) nos buscó, desarrollamos un micrositio completo y personalizable donde los usuarios podían seleccionar qué aplicaciones les interesaban para sus negocios y dispositivos celulares, para luego ver cómo funcionaban esas aplicaciones, cómo era la experiencia y cuánto dinero ahorrarían.</p>

featureData: []

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use Good workflow images
---
# My first blog post

Welcome to my first blog post using content module

---
name: Animación para la distribución en masa
slug: animation-for-mass-distribution
description: Animación para la distribución en masa
thumbnail: /assets/img/projects/animation-for-mass-distribution-cover.jpg
height: 720
width: 1280
categories: [Producción de video]
brand: Rafter
home: false
sliderImages: ['/assets/img/projects/animation-for-mass-distribution-slider-1.jpg']
video: Vp2N5SzscCY
featureText: Animaciones que cuentan buenas historias
services: [Producción de video, Diseño, Estrategia]

description1: <p>Rafter tenía un producto único y todas las oportunidades del mundo. Para promover esto de manera universal, ellos optaron por crear animaciones que contaran una mejor historia a su audiencia.</p>

featureData: []

description2: <p>Las animaciones fueron una manera simple de unir a la gente alrededor del mensaje, la oferta y todo lo referente al producto.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

---
name: Acercadndo las ofertas complejas
slug: making-complex-offerings-accessible
description: Acercadndo las ofertas complejas
thumbnail: /assets/img/projects/making-complex-offerings-accessible-cover.jpg
height: 800
width: 800
categories: [Diseño]
brand: Blackberry 
home: false
sliderImages: []
featureText: Convirtiendo lo aburrido en atractivo
services: [Diseño, Estrategia, Branding]

description1: <p>Blackberry (Good) es sinónimo de excelente tecnología, pero su tecnología puede resultar aburrida. Así que nos pusimos en la tarea de hacerla más interesante y atractiva para comprar.</p>

featureData: []

description2: <p>Diseñamos docenas de videos, experiencias web, sitios web, micrositios, folletos, casos de estudio y mucho más para impulsar la marca, los productos y las aplicaciones.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Blackberry Good_Workflow_Builder_5_6
---
# My first blog post

Welcome to my first blog post using content module

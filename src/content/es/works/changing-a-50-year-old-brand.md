---
name: Cambiando una marca con 50 años de historia
slug: changing-a-50-year-old-brand
description: Cambiando una marca con 50 años de historia
thumbnail: /assets/img/projects/changing-a-50-year-old-brand-cover.jpg
height: 720
width: 1280
categories: [Diseño]
brand: FICO
home: false
sliderImages: ['/assets/img/projects/changing-a-50-year-old-brand-slider-1.jpg']
featureText: Un diseño de sitio mucho mejor
services: [Diseño, Estrategia, Brandings]

description1: <p>FICO tiene una historia de 50 años y necesitaba que esta historia fuera contada a través de sus diseños. El sitio estaba un poco desactualizado y necesitaba una renovación, una nueva imagen y una nueva voz.</p>

featureData: []

description2: <p>Partimos de ahí, creando nuevas experiencias web con nuevos diseños basados en los insights sobre sus clientes. </p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Cannot use deck, but can use segments from it. Use various FICO Stuff sent to you by email
---
# My first blog post

Welcome to my first blog post using content module

---
name: Encontrando el bienestar con Jamba Juice
slug: finding-the-good-with-jamba-juice
description: Encontrando el bienestar con Jamba Juice
thumbnail: /assets/img/projects/finding-the-good-with-jamba-juice-cover.jpg
height: 720
width: 1280
categories: [Producción de video]
brand: Jamba Juice
home: false
sliderImages: ['/assets/img/projects/finding-the-good-with-jamba-juice-slider-1.jpg']
video: DYd1JKEhOuc
featureText: Las historias auténticas funcionan mejor
services: [Producción de video, Diseño, Estrategia]

description1: <p>Las campañas de Jamba Juice son reconocidas por promover la salud, el bienestar y una nueva identidad más 'nutritiva'.</p>

featureData: []

description2: <p>En dg trabajamos con la marca para crear historias auténticas que promueven estos valores y sus productos. .</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

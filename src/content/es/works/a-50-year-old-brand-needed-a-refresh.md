---
name: Una marca con 50 años que necesitaba un refresh
slug: a-50-year-old-brand-needed-a-refresh
description: Una marca con 50 años que necesitaba un refresh
thumbnail: /assets/img/projects/a-50-year-old-brand-needed-a-refresh-cover.jpg
height: 720
width: 1280
categories: [Interactivo/Web]
brand: FICO 
home: false
sliderImages: ['/assets/img/projects/a-50-year-old-brand-needed-a-refresh-slider-1.jpg']
featureText: Una imagen de marca más fuerte
services: [Interactivo/Web, Medición y Optimización, Diseño]

description1: <p>FICO tiene una historia de 50 años y necesitaba que esta historia se viera reflejada en sus diseños. El sitio estaba un poco desactualizado y necesitaba una renovación, una nueva imagen y una nueva voz.</p>

featureData: []

description2: <p>Partimos de ahí, creando nuevas experiencias web con nuevos diseños basados en los insights sobre sus clientes. </p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Cannot use deck, but can use segments from it. Use various FICO Stuff sent to you by email
---
# My first blog post

Welcome to my first blog post using content module

---
name: Expandiendo el negocio a una audiencia internacional
slug: expanding-to-an-interational-audience
description: Expandiendo el negocio a una audiencia internacional
thumbnail: /assets/img/projects/expanding-to-an-interational-audience-cover.jpg
height: 1325
width: 2000
categories: [Estrategia]
brand: Invisalign
home: true
order: <2>
sliderImages: ['/assets/img/projects/expanding-to-an-interational-audience-slider-1.jpg']
featureText: Comunicación para una experiencia de usuario global
services: [Estrategia, Branding, Diseño, Producción de video]

description1: <h4>DESAFÍO</h4> <br /> <p>Align Technology, líder mundial en soluciones de ortodoncia invisible, quería mejorar su relación con los clientes de América del Norte, Europa y Asia para ofrecer experiencias diferenciadas, excepcionales y consistentes. Align quería mejorar su capacidad de respuesta a las necesidades de sus clientes, aumentar la lealtad y elevar el Net Promoter Score, implementar mejoras en la experiencia del cliente y comprometerse con cambios en la cultura corporativa y la política de la empresa.</p>

featureData: ['1 NUEVA VISIÓN <small>para el futuro  del CX</small>', 'GLOBAL <small>desarrollo de marca</small>', 'EQUIPO COMERCIAL <small>herramientas para cerrar negocios</small>']

description2: <h4>IDEAS </h4> <p>desmond garcia ejecutó un mensaje global de experiencia del cliente para los mercados de América del Norte, Europa y Asia que ayudó a la empresa a conectarse mejor con sus clientes. Además de este gran esfuerzo estratégico, desmond garcía desarrolló una aplicación y contenido de video adicional para iPad para empoderar al equipo de ventas de América del Norte.</p> <h4>RESULTADOS</h4> <p>Esto que comenzó como un esfuerzo analítico, estratégico y creativo, ha dado como resultado el fortalecimiento de la capacidad de Align Technology para hacer negocios y empoderar a sus equipos de ventas.</p>

gallery: []
Source file: 
Notes: 
---

---
name: Volviendo simple lo complejo
slug: making-data-scientist-products-understandable
description: Volviendo simple lo complejo
thumbnail: /assets/img/projects/making-data-scientist-products-understandable-cover.jpg
height: 720
width: 1280
categories: [Producción de video]
brand: FICO
home: false
sliderImages: ['/assets/img/projects/making-data-scientist-products-understandable-slider-1.jpg']
video: 5MzM48SMzgI
featureText: Comunicación genial para una marca compleja
services: [Producción de video, Diseño, Estrategia]

description1: <p>FICO vende productos muy muy complejos. Para comercializarlos requieren de historias y activos de mercadeo que sean "cool", interesantes y fáciles de digerir.</p>

featureData: []

description2: <p> Nosotros tomamos estos productos y los convertimos en historias que cualquier persona, en cualquier tipo de situación pudiera comprender en esta serie de videos.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

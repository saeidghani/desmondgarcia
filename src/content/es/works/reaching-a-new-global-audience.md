---
name: Alcanzando una nueva audiencia global
slug: reaching-a-new-global-audience
description: Alcanzando una nueva audiencia global
thumbnail: /assets/img/projects/reaching-a-new-global-audience-cover.jpg
height: 720
width: 1280
categories: [Diseño]
brand: Invisalign 
home: false
sliderImages: ['/assets/img/projects/reaching-a-new-global-audience-slider-1.jpg']
featureText: Una experiencia de cliente global
services: [Diseño, Estrategia, Branding]

description1: <p>desmond garcia ejecutó una experiencia de usuario global en para los mercados de  Norte América, Europa y Asia lo cuál ayudó a la empresa a conectarse mejor con sus clientes. Además de este amplio esfuerzo estratégico, dg creó una aplicación para iPad que permite hablilitar ventas al equipo de ventas de Norte América y además permite ver contenido de video adicional.</p>

featureData: []

description2: <p>Esta interacción, que comenzó como una iniciativa analítica, estratégica y creativa, dio como resultado el fortalecimiento de la habilidad de Align Technology para hacer negocios y empoderar a los equipos de ventas.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use Invisalign CX Document
---
# My first blog post

Welcome to my first blog post using content module

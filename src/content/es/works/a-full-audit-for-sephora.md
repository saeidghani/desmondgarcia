---
name: Una auditoria completa para Sephora
slug: a-full-audit-for-sephora
description: Una auditoria completa para Sephora
thumbnail: /assets/img/projects/a-full-audit-for-sephora-cover.jpg
height: 720
width: 1280
categories: [Medición y Optimización]
brand: Sephora 
home: false
sliderImages: ['/assets/img/projects/a-full-audit-for-sephora-slider-1.jpg']
featureText: Insights convertidos en oportunidades
services: [Medición y optimización, Diseño, Estrategia]

description1: <p>desmond garcia hizo un análisis profundo del ecosistema digital de Sephora, proporcionando información cuantitativa sobre "dónde está todo y cómo está funcionando" basado en todos los tipos de categorías de contenido en todas las plataformas digitales, desde la web hasta redes sociales, tecnología móvil, correo electrónico y aplicaciones especializadas. Luego hicimos sugerencias específicas no solo para cada canal, sino también para cada medio/plataforma. </p>

featureData: []

description2: <p>Sephora experimentó un aumento intertrimestral del 4% en los ingresos. Nuestras ecomendaciones dieron como resultado un impulso exponencial en el tráfico al sitio web, mayor permanencia por usuario navegando en el sitio, una interacción móvil mejor y más profunda, y una historia de marca más completa.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use Sephora Case Study
---
# My first blog post

Welcome to my first blog post using content module

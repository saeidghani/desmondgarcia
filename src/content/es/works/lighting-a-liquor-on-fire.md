---
name: Prendiendo fuego a un licor
slug: lighting-a-liquor-on-fire
description: Prendiendo fuego a un licor
thumbnail: /assets/img/projects/lighting-a-liquor-on-fire-cover.jpg
height: 720
width: 1280
categories: [Diseño]
brand: EJ Gallo 
home: false
sliderImages: ['/assets/img/projects/lighting-a-liquor-on-fire-slider-1.jpg']
featureText: Prendiendo fuego a un licor
services: [Diseño, Estrategia, Branding]

description1: <p>desmond garcia conoce a los Millennials y la Generación Y, ambos públicos objetivo actuales y futuros. En dg entendemos que en el ZMOT (punto cero de la verdad), el sitio "mostrará y contará".  Nuestro público objetivo ama las experiencias auténticas, las redes sociales y desea conocer sobre la oferta de nuevos productos y sobre las diferentes maneras de utilizarlos. Entonces la experiencia .com fue diseñada para ser limpia, pensada para móvil y con contenido que la audiencia quisiera "pinear", "postear", "tuitear" y compartir con el mundo. También creamos una experiencia de marca auténtica por medio de la construcción de un micrositio donde los usuarios pudieran publicar su propio contenido generado por ellos mismos y creamos calendarios de redes sociales y posts que se vincularan sin mayor costo, a través de los canales de las redes sociales tradicionales.</p>

featureData: []

description2: <p>Después de muchos meses de diseño, desarrollo y activación, el tráfico, las interacciones y lo más importante, las ventas, se triplicaron. Gallo creó un SKU adicional para la marca, dando como resultado aun mayores ventas y contenido adicional. Este fue un esfuerzo de toda la agencia, desde estrategia, marca, diseño, producción de video y fotografía, manejo de redes sociales y otras tareas.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: EJ Gallo Case Study
---
# My first blog post

Welcome to my first blog post using content module

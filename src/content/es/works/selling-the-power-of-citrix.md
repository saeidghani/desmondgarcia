---
name: Vendiendo el poder de Citrix
slug: selling-the-power-of-citrix
description: Vendiendo el poder de Citrix
thumbnail: /assets/img/projects/selling-the-power-of-citrix-cover.jpg
height: 720
width: 1280
categories: [Producción de video]
brand: Citrix
home: true
sliderImages: ['/assets/img/projects/selling-the-power-of-citrix-slider1.jpg']
video: db03hxuX3io
featureText: Un video de marca muy limpio y simple
services: [Producción de video, Diseño, Estrategia]

description1: <p> Cuando Citrix buscó ayuda para producir una serie de videos y convertir los activos existentes en contenido comercializable, nosotros respondimos. Creamos docenas de videos donde se discutía la propuesta de la marca y la oferta de producto--videos que fueron distribuidos a través de múltiples puntos de venta.</p>

featureData: []

description2: <p> Video de marca de Citrix: un video muy limpio y simple con material audiovisual original y detalles mínimos en postproducción y en gráficos. Eso es lo que entregamos felizmente para esta gran marca.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

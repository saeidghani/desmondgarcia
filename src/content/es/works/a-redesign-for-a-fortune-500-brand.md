---
name: Un rediseño para una marca de Fortune 500
slug: a-redesign-for-a-fortune-500-brand
description: Un rediseño para una marca de Fortune 500
thumbnail: /assets/img/projects/a-redesign-for-a-fortune-500-brand-cover.jpg
height: 720
width: 1280
categories: [Interactivo/Web]
brand: Maersk 
home: false
sliderImages: ['/assets/img/projects/a-redesign-for-a-fortune-500-brand-slider-1.jpg']
featureText: Conectando mejor con las audiencias
services: [Interactivo/Web, Medición y Optimización, Diseño]

description1: <p>Cuando Maersk buscaba actualizar su sitio web y crear una nueva experiencia de usuario que contara una buena historia, con imágenes, videos y una nueva dirección de marca, recurrieron a nosotros.</p>

featureData: []

description2: <p>El resultado fue un sitio nuevo y espectacular que conectó emocionalmente con el público y mejoró los resultados.</p>

gallery: []
Source file: Pull screen shots from here:https://www.maersklinelimited.com/
Notes: Pull screen shots from here:https://www.maersklinelimited.com/
---
# My first blog post

Welcome to my first blog post using content module

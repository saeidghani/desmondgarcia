---
name: Creando teasers para iniciar conversaciones
slug: creating-teasers-to-ignite-conversations
description: Creando teasers para iniciar conversaciones
thumbnail: /assets/img/projects/creating-teasers-to-ignite-conversations-cover.jpg
height: 800
width: 800
categories: [Producción de video]
brand: FICO
home: false
sliderImages: ['/assets/img/projects/creating-teasers-to-ignite-conversations-slider-1.jpg']
video: BMSNgExnTIo
featureText: Construyendo afinidad a través del video
services: [Producción de video, Diseño, Estrategia]

description1: <p>Los teasers y videos cortos son una gran manera de construir afinidad con una marca. Nosotros produjimos y grabamos muchos de estos para FICO con el fin de ayudarle a sus científicos de datos a vender con mayor facilidad. </p>

featureData: []

description2: <p>FICO vende productos muy muy complejos. Para comercializarlos requieren de historias y activos de mercadeo que sean "cool", interesantes y fáciles de digerir. Nosotros tomamos estos productos que son súper complejos y los convertimos en historias que cualquier persona, en cualquier tipo de situación pudiera comprender en esta serie de videos.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

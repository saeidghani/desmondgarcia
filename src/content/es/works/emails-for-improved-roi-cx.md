---
name: Usando correos electrónicos para un ROI y experiencia de usuario mejorados
slug: emails-for-improved-roi-cx
description: Usando correos electrónicos para un ROI y experiencia de usuario mejorados
thumbnail: /assets/img/projects/emails-for-improved-roi-cx-cover.jpg
height: 720
width: 1280
categories: [Diseño]
brand: Sephora 
home: false
sliderImages: ['/assets/img/projects/emails-for-improved-roi-cx-slider-1.jpg']
featureText: Mayores tasas de apertura y conversión
services: [Diseño, Estrategia, Branding]

description1: <p>desmond garcia hizo un análisis profundo del ecosistema digital de Sephora, proporcionando información cuantitativa sobre "dónde está todo y cómo está funcionando" basado en todos los tipos de categorías de contenido en todas las plataformas digitales, desde la web hasta redes sociales, tecnología móvil, correo electrónico y aplicaciones especializadas.</p>

featureData: []

description2: <p>Luego hicimos sugerencias específicas no solo para cada canal, sino también para cada medio/plataforma. Continuamos con nuevos diseños, nuevas plantillas de correo electrónico, nuevos formatos y diseños que dieron como resultado mayores tasas de apertura y conversión.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Sephora Case Study In Deck
---
# My first blog post

Welcome to my first blog post using content module

---
name: Inspirando las audiencias de las conferencias
slug: inspiring-conference-audiences
description: Inspirando las audiencias de las conferencias
thumbnail: /assets/img/projects/inspiring-conference-audiences-cover.jpg
height: 720
width: 1280
categories: [Producción de video]
brand: FICO
home: false
sliderImages: ['/assets/img/projects/inspiring-conference-audiences-slider-1.jpg']
video: 1lu17euYiw4
featureText: Inyectando storytelling a cada activo de marketing
services: [Producción de video, Diseño, Estrategia]

description1: <p>FICO vende productos muy muy complejos. Para comercializarlos requieren de historias y activos de mercadeo que sean "cool", interesantes y fáciles de digerir.</p>

featureData: []

description2: <p> Nosotros tomamos estos productos y los convertimos en historias que cualquier persona, en cualquier tipo de situación pudiera comprender en esta serie de videos.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

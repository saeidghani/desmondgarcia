---
slug: marketing1
location: Remote, Col
date: Apr 5 - 2021
category: Marketing
title: Senior Digital Strategist  
summary: '
<p>
<strong>desmond garcia</strong> 
is an established strategic and marketing agency founded in 2019 with personnel working globally from Los Angeles, to Miami to a large base in Medellin. We are growing internationally and are looking for a Senior Digital Strategist.  
<br>
This role will support and lead a variety of client-facing brand and strategic efforts and grow the brand and strategic branch of <strong>desmond garcia</strong> for 2021
</p>
'
content: 'The Senior Digital Strategist  is responsible for managing multiple internal, new business and client projects and is expected to effectively collaborate with Developmental, Creative, Brand Management, and Digital Innovation teams. The Senior Digital Strategist  will run consulting and strategy projects independently.
 <br>
The success in this role demands a strong strategic and brand capabilities and analytical orientation coupled with exceptional communication skills as well as a highly developed collaborative nature. Vision, energy, enthusiasm and focus are required but also contextual sensitivity and the ability to work effectively across a broad spectrum of people.
 <br>
Our clients are some of the world’s top brands  and Fortune & Dinero 500 companies. Like Google, Wells Fargo, Unilever, Visa, Sephora,  DocuSign, Visa, Heineken, GSK, Comcast NBCU, and Nestle. 
 <br>
Some of the services we offer are: Digital marketing & Strategy, Video production, Branding, Design and Corporate philanthropy
 <br>
<strong>
The Senior Digital Strategist will be responsible for, but not limited to:
</strong>
<h4>Digital Strategy: </h4> 

<ul>
   <li>
      Build our digital marketing roadmap
   </li>
   <li>
      Develop Digital Marketing roadmaps and strategies for clients and for desmond garcia as a brand 
   </li>
   <li>
      Plan, develop, implement and manage the overall digital marketing strategy for desmond garcia and lead client projects. 
   </li>
   <li>
      Manage all digital marketing channels (e.g. website, blogs, emails and social media) to ensure brand consistency
   </li>
   <li>
      Oversee our social media accounts and those of clients when project scopes requiere it to ensure that social is continuously growing and that social content is forward thinking and engaging
   </li>
   <li>
      Stay up-to-date with digital technology developments
   </li>
</ul>

<h4>Branding:</h4>
<ul>
   <li>
      Run research and strategy projects for clients including (but not limited to): competitive analysis, Brand DNA creation, positioning pyramids, brand tone of voice/ verbal identity and Brand Book creation.
   </li>
   <li>
      Responsible for leading the company with all projects outwardly facing brand projects for high-level clients that require brand development, including voice, identity, positioning, vision and more
   </li>
   <li>
      The Senior Digital Strategist  is responsible over all digital creative outputs inclusive of web promotional materials, selection of creative agencies, creative briefings, graphics, photography across the business.
   </li>
   <li>
      The Senior Digital Strategist  is responsible for the creation and placement of advertising that is in support of the business’s and sales team’s initiatives through media planning, creative development, metrics, market research, and vendor management.
   </li>
   <li>
      The Senior Digital Strategist will be responsible for the creation of all forms of marketing  example: interactive marketing, direct marketing campaigns, advertising, sales collateral, press releases, and executive presentations
   </li>
   <li>
      Provide inspiration and direction for the teams in regular creative sessions.
   </li>
   <li>
      To challenge and influence at a senior level the direction of the Brand, bringing ideas, inspiration and creative thinking.
   </li>
   <li>
      The Head of Brand Marketing also ensures the completion of market analyses and monitors the competitive activity in the market as well as conducting gap analyses and financial modeling
   </li>
   <li>
      Works with brand campaign managers in ensuring relevant brand promotion that will lead to the achievement of maximum ROI.
   </li>
   <li>
      Creating & executing successful brand campaigns
   </li>
   <li>
      Working with the Director on unique, groundbreaking and rebranding projects.
   </li>
   <li>
      Guiding graphic designers to ensure content is on brand and on time
   </li>
   <li>
      Supporting social media working with design and buying to plan campaigns around launches and key dates in the year
   </li>
   <li>
      Building brand deck and guidelines to drive consistency through the business
   </li>
</ul>


<h4>General:</h4>
<ul>
   <li>
      Building relationships with all ambassadors and key influencers
   </li>
   <li>
      Bringing on board new collaborators
   </li>
   <li>
      Ensure all projects are on brand, budget and to meet deadlines .
   </li>
   <li>
      The Senior Digital Strategist  plays a mentorship role to key personnel in the brand marketing department, honing their professional skills and preparing them to occupy this position in the future.
   </li>
</ul>

<h4>You should:</h4>
<ul>
   <li>
      Be highly motivated and organised
   </li>
   <li>
      Have In-depth marketing experience and a passion for digital technologies
   </li>
   <li>
      Be a good strategist who is able to develop plans and bring to fruition
   </li>
   <li>
      Be a strong multitasker who is able to keep all teams reporting to them working effectively
   </li>
   <li>
      Be extremely creative
   </li>
   <li>
      have to have exceptional leadership skills.
   </li>
   <li>
      Actively take an interest in social and cultural trends, developments in digital sector and related industries
   </li>
   <li>
      Be comfortable  about the prospect of working directly with multiple clients and new business pitches at any given time.
   </li>
</ul>

<h3>Experience Essential</h3>
<h4>General:</h4>
<ul>
   <li>
      Have 10+ years of relevant advertising/digital marketing experience. 
   </li>
   <li>
      Graduate degree in marketing, communications, advertising.
   </li>
   <li>
      Diploma or postgraduate degree in digital marketing 
   </li>
   <li>
      Experience of working and reporting to Boards and Committees
   </li>
</ul>

<h4>Digital:</h4>
<ul>
   <li>
      Experience in Digital Marketing and Brand strategy is a must 
   </li>
   <li>
      Demonstrable experience in designing and implementing successful digital marketing campaigns
   </li>
   <li>
      Strong understanding of how all current digital marketing channels function
   </li>
   <li>
      Solid knowledge of online marketing tools and best practices
   </li>
   <li>
      Hands on experience with SEO/SEM, Google Analytics and CRM software
   </li>
   <li>
      Familiarity with web design - understanding of UX/UI and web analytics
   </li>
   <li>
      Experience with content marketing 
   </li>
   <li>
      Experience with performance marketing 
   </li>
   <li>
      Experience with digital advertising 
   </li>
   <li>
      Wanted: Experience working in marketing, advertising or media agencies
   </li>
</ul>


<h4>Branding:</h4>
<ul>
   <li>
      A good record of achievement at a senior level in the field of brand strategy, especially in the digital sector or an industry of similar scale and complexity.
   </li>
   <li>
      Senior experience in directing a brand team to deliver unified messages to targeted audiences, based on the highest creative standards, and sound creative judgment
   </li>
   <li>
      Senior experience in managing global brands, which manifest differently in different markets around the world. Commercial experience is also desirable.
   </li>
   <li>
      Experience in developing brand identities
   </li>
   <li>
      Have a full understanding of audiences and ability to respond and anticipate in an ever changing media landscape
   </li>
   <li>
      Proven ability to clearly convey information and instructions, which will determine the effectiveness with which strategies are executed 
   </li>
   <li>
      Evidence of success in a results driven environment 
   </li>
   <li>
      Strong presentational and influencing skills 
   </li>
   <li>
      Track record of successfully delivering change 
   </li>
   <li>
      Experience of developing and implementing guidelines and working processes in a broad range of areas
   </li>
</ul>


<h4>General:</h4>
<ul>
   <li>
      Excellent communication skills for the delivery of regular reports for the business’s leadership, which must be easily digestible, unambiguous, engaging, informative and convincing.
   </li>
   <li>
      Significant experience of managing commercial and matrix relationships in a large/comparable organisation.
   </li>
   <li>
      Leading, coaching and managing employees
   </li>
   <li>
      Working and reporting to Boards and Committees 
   </li>
</ul>



<h4>About desmond  garcia:</h4>


<p>
   desmond garcia is an integrated marketing and innovation agency that is quickly growing. Our efforts are based in four pillars: Content Creation, Storytelling, Design, and Experience, all which manifest in our following offerings:
</p>

<p>
   • Qualitative audience research • Brand positioning and planning • Brand story & voice development  • Brand messaging development • Content strategy • Content development • User experience design • Visual design • Integrated campaigns • Video strategy • Video production 
</p>

<p>
   Please send your resume to:  <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com </a>
</p>
<p>
   This is a full-time executive position. Initial salary negotiable based on experience.
</p>'
---




---
slug: design1
location: Miami, FL
date: Mar 3 - 2021
category: Design
title: Mid level Graphic Designer
summary: '
<p>
<strong>desmond garcia </strong>
is an established strategic and marketing agency founded in 2019 with personnel working globally from Los Angeles, to Miami to a large base in Medellin. We are growing internationally and are looking for a Graphic Designer with Ui & Ux experience.
</p>
<p>
We are seeking committed, motivated individuals to join our hard-working leaders to make it happen in a dynamic growing organization and enjoy being part of a talented team. You should capture the attention of those who see them and communicate the right message, for this, you need to have a creative flair and a strong ability to translate requirements into design. 
</p>
<p>
If you can communicate well and work methodically as part of a team, we’d like to meet you. The goal is to inspire and attract the target audience.
</p>
'

content: '

<h4>Requirements and qualifications</h4>
<ul>
   <li>
      Graphic designer with 4 + years of experience with Degree in Design, Fine Arts or related field.
   </li>
   <li>
      Demonstrated experience in graphic creation, branding, content creation, Web design, Ui design, Infographics design, Mockups.
   </li>
   <li>
      Strong knowledge Photoshop, Illustrator, InDesign, Adobe XD, Invision, Figma, Sketch.
   </li>
   <li>
      Test graphics across various media and Amend designs after feedback.
   </li>
   <li>
      Experience in marketing campaign projects and designs is a big plus.
   </li>
   <li>
      Is a plus if you have UI/UX knowledge and experience.
   </li>
   <li>
      Knowledge in HTML, CSS, Less, Sass is a big plus.
   </li>
</ul>
<h4>Your responsibilities</h4>
<ul>
   <li>
      Conceptualizing visuals based, Study design briefs and determine requirements
   </li>
   <li>
      Create interactive design that drives our marketing lead generation and product awareness.
   </li>
   <li>
      Responsible for working side by side with our PMs and front-end developers to create design solutions that have a high visual impact for our marketing campaigns.
   </li>
   <li>
      Communicating with clients and development team, Researching interactive design trends, In charge of conceptualization and visual communication of the brand and campaigning.
   </li>
</ul>
<h4>Soft Skills</h4>
<ul>
   <li>
      Great interpersonal, presentation and communication skills
   </li>
   <li>
      Intermediate- to Advance English Level
   </li>
   <li>
      A keen eye for aesthetics and details
   </li>
   <li>
      Ability to work methodically and meet deadlines
   </li>
</ul>
<p>
   You will work closely with the Product Designer UX/UI, Business Development team, PR teams, designers, developers, video producer, Project Manager 
</p>
<h4>About Desmond I Garcia:</h4>
<p>
   Desmond I Garcia is an integrated marketing and innovation agency. Our efforts are based in four pillars; Content Creation, Storytelling, Design, and Experience, all which manifest in our following offerings: 
</p>
<p>
   Qualitative audience research • Brand positioning and planning • Brand story &amp; voice development • Brand messaging development • Content strategy • Content development • User experience design • Visual design • Integrated campaigns • Video strategy • Video production 
</p>
<p>
   Please send your resume to: <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com </a> 
</p>
<p>
This is a Freelance Job based project. <br> Initial salary negotiable based on experience. <br> Contract: Prestación de Servicios
</p>'
---




---
name: A full identity & brand rebirth
slug: a-full-identity-brand-rebirth
description: A full identity & brand rebirth
thumbnail: /assets/img/projects/a-full-identity-brand-rebirth-cover.jpg
height: 800
width: 800
categories: [Branding]
brand: EJ Gallo
home: false
sliderImages: []
featureText:
services: [Strategy, Branding, Design]

description1: <p>Due to the sensitive nature of branding we do not share brand work publicly. Here is a list of clients we've collaborated with on audience, personas, brand work, development & positioning.</p>

featureData: []

description2:

gallery: []
Source file: https://drive.google.com/file/d/0B106BJcGmDdsZ1N6VWNuc0FGdkk/view?usp=sharing
Notes: Not sure what to do here. Open to suggestions
---
# My first blog post

Welcome to my first blog post using content module

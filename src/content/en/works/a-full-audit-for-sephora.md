---
name: A full audit for Sephora
slug: a-full-audit-for-sephora
description: A full audit for Sephora
thumbnail: /assets/img/projects/a-full-audit-for-sephora-cover.jpg
height: 720
width: 1280
categories: [Measurement & Optimization]
brand: Sephora 
home: false
sliderImages: ['/assets/img/projects/a-full-audit-for-sephora-slider-1.jpg']
featureText: Insights turned into opportunities
services: [Measurement & Optimization, Design, Strategy]

description1: <p>desmond garcia did a deep dive within Sephora’s digital ecosystem, providing quantitative insights around “Where is everything & how is it performing” based off of all content category types in all digital platforms, from web, to social, to mobile and specialty applications. Next, we made specific suggestions for not only each channel, but also each media outlet/platform. These recommendations were based on existing consumer trends and future trends, like engagement via video, microsites, Snapchat and other platforms which may have been underutilized but certainly were effective and on the rise.</p>

featureData: []

description2: <p>Sephora saw a 4% QOQ rise in revenue. That’s where it counts the most. Leveraging our discoveries and recommendations also resulted in an exponential drive in traffic through the site, more time spent per user on the site, better and deeper mobile engagement, and a more comprehensive brand story that works across the entire company.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use Sephora Case Study
---
# My first blog post

Welcome to my first blog post using content module

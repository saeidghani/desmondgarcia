---
name: Making complex offerings accessible
slug: making-complex-offerings-accessible
description: Making complex offerings accessible
thumbnail: /assets/img/projects/making-complex-offerings-accessible-cover.jpg
height: 800
width: 800
categories: [Design]
brand: Blackberry 
home: false
sliderImages: []
featureText: Turning the boring into appealing
services: [Design, Strategy, Branding]

description1: <p>Blackberry (Good) stands for great technology, but this technology can be boring. So we set off to make it more appealing to buy and attractive.</p>

featureData: []

description2: <p>We designed dozens of videos, web experiences, websites, microsites, brochures, case studies and more to lift the brand, products and applications.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Blackberry Good_Workflow_Builder_5_6
---
# My first blog post

Welcome to my first blog post using content module

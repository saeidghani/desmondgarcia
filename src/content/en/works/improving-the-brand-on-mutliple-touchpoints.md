---
name: Improving the brand on mutliple touchpoints
slug: improving-the-brand-on-mutliple-touchpoints
description: Improving the brand on mutliple touchpoints
thumbnail: /assets/img/projects/improving-the-brand-on-mutliple-touchpoints-cover.jpg
height: 800
width: 800
categories: [Design]
brand: DocuSign
home: true
sliderImages: ['/assets/img/projects/improving-the-brand-on-mutliple-touchpoints-slider-1.jpg']
video: JFKJ5lm-1t0
featureText: Creating all types of content
services: [Design, Strategy, Branding]

description1: '<p>desmond garcia brought DocuSign our core approach: overarching brand and content strategies, followed by design, video production, interactive assets and content creation. Since working with DocuSign, we’ve created broad content strategies, specific content strategies for campaigns, how-to videos, anthem videos, sizzle videos, product videos, messaging, micro-sites, and have supported DocuSign with the creation of all types of content and design assets for its major conferences, events and acquisitions.</p>'

featureData: ['IMMEDIATE 4% <small>quarterly revenue growth<small>', 'DOUBLE DIGIT <small>YOY growth<small>', 'DEFINED SEPHORA <small>as a “digital disruptor”<small>']

description2: <p>The results speak for themselves:684% increase in new users, adoption by all enterprise businesses in the Fortune 100, and an increase from $220M to $41.35B company value.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use something you've designed...
---

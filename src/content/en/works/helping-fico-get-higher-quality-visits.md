---
name: Helping FICO get higher quality visits
slug: helping-fico-get-higher-quality-visits
description: Helping FICO get higher quality visits
thumbnail: /assets/img/projects/helping-fico-get-higher-quality-visits-cover.jpg
height: 720
width: 1280
categories: [Measurement & Optimization]
brand: FICO 
home: false
sliderImages: ['/assets/img/projects/helping-fico-get-higher-quality-visits-slider-1.jpg']
featureText: Turning visitors into customers
services: [Measurement & Optimization, Design, Strategy]

description1: <p>FICO's work with clients is enormous. The brand is the data and science that fuels massive amounts of digital and in-the-flesh experiences.</p>

featureData: []

description2: <p>In order to help improve their CX, we assisted FICO with analytics and understanding next steps to take to engage their customers for more time and turn early stage visitors into customers.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: There's no photo asset for this as it's measurement & optimization
---
# My first blog post

Welcome to my first blog post using content module

---
name: Making data scientist products understandable
slug: making-data-scientist-products-understandable
description: Making data scientist products understandable
thumbnail: /assets/img/projects/making-data-scientist-products-understandable-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: FICO
home: false
sliderImages: ['/assets/img/projects/making-data-scientist-products-understandable-slider-1.jpg']
video: 5MzM48SMzgI
featureText: Cool communication for a complex brand
services: [Video Production, Design, Strategy]

description1: <p>FICO <small> sells really really really complex products. To market them, they need storytelling and marketing assets which are cool, interesting and easy to digest. </p>

featureData: []

description2: <p> We took these very complex products and turned them into stories that anyone from any type of position could understand in this series of videos.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

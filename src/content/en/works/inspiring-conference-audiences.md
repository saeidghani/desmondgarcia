---
name: Inspiring conference audiences
slug: inspiring-conference-audiences
description: Inspiring conference audiences
thumbnail: /assets/img/projects/inspiring-conference-audiences-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: FICO
home: false
sliderImages: ['/assets/img/projects/inspiring-conference-audiences-slider-1.jpg']
video: 1lu17euYiw4
featureText: Injecting storytelling to every asset.
services: [Video Production, Design, Strategy]

description1: <p>FICO <small> sells really really really complex products. To market them, they need storytelling and marketing assets which are cool, interesting and easy to digest.</p>

featureData: []

description2: <p> We took these very complex products and turned them into stories that anyone from any type of position could understand in this series of videos.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

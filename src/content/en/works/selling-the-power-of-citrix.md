---
name: Selling the Power of Citrix
slug: selling-the-power-of-citrix
description: Selling the Power of Citrix
thumbnail: /assets/img/projects/selling-the-power-of-citrix-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: Citrix
home: true
sliderImages: ['/assets/img/projects/selling-the-power-of-citrix-slider1.jpg']
video: db03hxuX3io
featureText: We are experts in bringing <br> brands to life digitally.
services: [Video Production, Design, Strategy]

description1: <p> When Citrix reached out for help to produce a series of brand videos and turn existing assets into marketable content, we responded. We created dozens of videos that discussed the brand proposition and product offering--videos which were distributed across mutliple distribution outlets.</p>

featureData: []

description2: <p> Citrix campaign brand video:A very clean, simple video with all original footage and minimal details on post production & graphics. That's what we happily delivered for this great brand.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

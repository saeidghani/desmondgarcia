---
name: A 50 year old brand needed a refresh
slug: a-50-year-old-brand-needed-a-refresh
description: A 50 year old brand needed a refresh
thumbnail: /assets/img/projects/a-50-year-old-brand-needed-a-refresh-cover.jpg
height: 720
width: 1280
categories: [Interactive/Web]
brand: FICO 
home: false
sliderImages: ['/assets/img/projects/a-50-year-old-brand-needed-a-refresh-slider-1.jpg']
featureText: A stronger brand image
services: [Interactive/Web, Measurement & Optimization, Design]

description1: <p>FICO has a 50 year history and needed that history to show in it's designs. The site was somewhat outdated and needed refreshing, a new look and a new voice.ac</p>

featureData: []

description2: <p>We took it from there, creating new web experiences with new designs based off of customer insights. </p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Cannot use deck, but can use segments from it. Use various FICO Stuff sent to you by email
---
# My first blog post

Welcome to my first blog post using content module

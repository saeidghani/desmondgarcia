---
name: Changing a 50 year old brand
slug: changing-a-50-year-old-brand
description: Changing a 50 year old brand
thumbnail: /assets/img/projects/changing-a-50-year-old-brand-cover.jpg
height: 720
width: 1280
categories: [Design]
brand: FICO
home: false
sliderImages: ['/assets/img/projects/changing-a-50-year-old-brand-slider-1.jpg']
featureText: A much better site design
services: [Design, Strategy, Branding]

description1: <p>FICO has a 50 year history and needed that history to show in it's designs. The site was somewhat outdated and needed refreshing, a new look and a new voice.</p>

featureData: []

description2: <p>We took it from there, creating new web experiences with new designs based off of customer insights.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Cannot use deck, but can use segments from it. Use various FICO Stuff sent to you by email
---
# My first blog post

Welcome to my first blog post using content module

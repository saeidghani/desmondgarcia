---
name: Emails for improved ROI & CX
slug: emails-for-improved-roi-cx
description: Emails for improved ROI & CX
thumbnail: /assets/img/projects/emails-for-improved-roi-cx-cover.jpg
height: 720
width: 1280
categories: [Design]
brand: Sephora 
home: false
sliderImages: ['/assets/img/projects/emails-for-improved-roi-cx-slider-1.jpg']
featureText: Higher open and conversion rates
services: [Design, Strategy, Branding]

description1: <p>desmond garcia did a deep dive within Sephora’s digital ecosystem, providing quantitative insights around “Where is everything & how is it performing” based off of all content category types in all digital platforms, from web, to social, to mobile, email and specialty applications.</p>

featureData: []

description2: <p>Next, we made specific suggestions for not only each channel, but also each media outlet/platform. We followed up with new designs, new email templates, formats and layouts which resulted in higher open and conversion rates.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Sephora Case Study In Deck
---
# My first blog post

Welcome to my first blog post using content module

---
name: Inspiring conference audiences & partnerships
slug: inspiring-conference-audiences-partnerships
description: Inspiring conference audiences & partnerships
thumbnail: /assets/img/projects/inspiring-conference-audiences-partnerships-cover.jpg
height: 800
width: 800
categories: [Video Production]
brand: TED
home: false
sliderImages: ['/assets/img/projects/inspiring-conference-audiences-partnerships-slider-1.jpg']
video: tPAf1xC2odI
featureText: Communicating female power.
services: [Video Production, Design, Strategy]

description1: <p>desmond garcia partnered with TED to film a series of videos for TEDWomen to discuss the important role of diversity in business and the power of female mentorship with top executives from around the country.</p>

featureData: []

description2:

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

---
name: New sites for a category leader
slug: new-sites-for-a-category-leader
description: New sites for a category leader
thumbnail: /assets/img/projects/new-sites-for-a-category-leader-cover.jpg
height: 720
width: 1280
categories: [Interactive/Web]
brand: DocuSign 
home: false
sliderImages: []
featureText: Taking a brand to the next level
services: [Interactive/Web, Measurement & Optimization, Design]

description1: <p>Where do we begin? Over the years we've crafted dozens of websites and interactive epxeriences for DocuSign--from conferences, to verticals, we've designed and built portions of their digital experiences for years.</p>

featureData: []

description2: <p>The result has been a pleasure and seeing the rise of our friends grow from private to public company and industry leader.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use something you've designed.
---
# My first blog post

Welcome to my first blog post using content module

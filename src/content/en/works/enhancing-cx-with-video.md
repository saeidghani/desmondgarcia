---
name: Enhancing CX with video
slug: enhancing-cx-with-video
description: Enhancing CX with video
thumbnail: /assets/img/projects/enhancing-cx-with-video-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: 23andMe
home: false
sliderImages: ['/assets/img/projects/enhancing-cx-with-video-slider-1.jpg']
video: m_BMytyLtOs
featureText: Demo videos for better understanding  
services: [Video Production, Design, Strategy]

description1: <p>We conceptualized, produced, filmed, edited and finished a series of 6 videos designed to help new customers with their product experiences and expectations. This built brand affinity and helped turn new customers into brand ambassadors.</p>

featureData: []

description2:

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes:
---
# My first blog post

Welcome to my first blog post using content module

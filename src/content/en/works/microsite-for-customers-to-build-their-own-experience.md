---
name: Microsite for customers to build their own experience
slug: microsite-for-customers-to-build-their-own-experience
description: Microsite for customers to build their own experience
thumbnail: /assets/img/projects/microsite-for-customers-to-build-their-own-experience-cover.jpg
width: 800
height: 800
categories: [Interactive/Web]
brand: Blackberry 
home: false
sliderImages: []
featureText: Personalized customer experience
services: [Interactive/Web, Measurement & Optimization, Design]

description1: <p>When Blackberry (Good) reached out to us, we developed a comprehensive, customizable microsite where users could visit, select what applications they were interested in for their businesses and cellular devices, then see how those apps would work, what the experience was like, and how much money they would save. </p>

featureData: []

description2: 

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use Good workflow images
---
# My first blog post

Welcome to my first blog post using content module

---
name: Firing up conference audiences
slug: firing-up-conference-audiences
description: Firing up conference audiences
thumbnail: /assets/img/projects/firing-up-conference-audiencess-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: DocuSign
home: false
sliderImages: ['/assets/img/projects/firing-up-conference-audiencess-slider-1.jpg']
video: 5hnnDk28N58
featureText: An opener video for an important conference
services: [Video Production, Design, Strategy]

description1: <p>Conferences are key to many companies, whether they're Silicon Valley, new or established. We created many conference videos for DocuSign's annual MOMENTUM conference.</p>

featureData: []

description2: <p>The intention is to fire up audiences and build brand affinity before and during the event.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

---
name: Combining Google and DocuSign
slug: combining-google-and-docusign
description: Combining Google and DocuSign
thumbnail: /assets/img/projects/combining-google-and-docusign-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: DocuSign
home: false
sliderImages: ['/assets/img/projects/combining-google-and-docusign-slider-1.jpg']
video: qGuYT8n9yg8
featureText: Adding value with storyteling.
services: [Video Production, Design, Strategy]

description1: <p>When Google partnered with DocuSign, they reached out to us to help tell the story of the uniting of their products.</p>

featureData: []

description2: <p>Our goal was to take something that was digital and potentially flat and add a story to it which would resonate with customers from any and every background.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

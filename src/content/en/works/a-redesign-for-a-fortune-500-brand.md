---
name: A redesign for a Fortune 500 brand
slug: a-redesign-for-a-fortune-500-brand
description: A redesign for a Fortune 500 brand
thumbnail: /assets/img/projects/a-redesign-for-a-fortune-500-brand-cover.jpg
height: 720
width: 1280
categories: [Interactive/Web]
brand: Maersk 
home: false
sliderImages: ['/assets/img/projects/a-redesign-for-a-fortune-500-brand-slider-1.jpg']
featureText: Connecting better with the audiences
services: [Interactive/Web, Measurement & Optimization, Design]

description1: <p>When Maersk was looking to update their former website and create a new UX that told more of a story, with imagery, video and a new brand direction, they turned to us. </p>

featureData: []

description2: <p>The result was a spectacular new site that connected emotionally with audiences and improved the bottom line.</p>

gallery: []
Source file: Pull screen shots from here:https://www.maersklinelimited.com/
Notes: Pull screen shots from here:https://www.maersklinelimited.com/
---
# My first blog post

Welcome to my first blog post using content module

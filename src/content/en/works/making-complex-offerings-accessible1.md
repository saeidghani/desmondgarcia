---
name: Making complex offerings accessible
slug: making-complex-offerings-accessible1
description: Making complex offerings accessible1
thumbnail: /assets/img/projects/making-complex-offerings-accessible1-cover.jpg
height: 800
width: 800
categories: [Measurement & Optimization]
brand: Blackberry
home: false
sliderImages: []
featureText: Customizable analytics reports
services: [Measurement & Optimization, Design, Strategy]

description1: <p>Blackberry (Good) is all about marketing data, understanding the data, and turning it into actionable insights. We worked with Blackberry to better understand the analytics and understanding Google's findings and next steps to take to engage their customers for more time and turn early stage visitors into customers.</p>

featureData: []

description2: <p>We built customizable analytics reports and applications within proprietary experiences which empowered the sales teams to reach out with more data and convert customers better.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Blackberry Good_Workflow_Builder_5_6
---
# My first blog post

Welcome to my first blog post using content module

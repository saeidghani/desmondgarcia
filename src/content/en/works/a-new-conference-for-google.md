---
name: A new conference for Google
slug: a-new-conference-for-google
description: A new conference for Google
thumbnail: /assets/img/projects/a-new-conference-for-google-cover.jpg
width: 800
height: 800
categories: [Branding]
brand: Google
home: false
sliderImages: []
featureText:
services: [Strategy]

description1: <p>Due to the sensitive nature of branding we do not share brand work publicly. Here is a list of clients we've collaborated with on audience, personas, brand work, development & positioning.</p>

featureData: []

description2:

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Not sure what to do here. Open to suggestions
---
# My first blog post

Welcome to my first blog post using content module

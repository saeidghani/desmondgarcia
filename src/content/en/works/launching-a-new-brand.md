---
name: Launching a new brand
slug: launching-a-new-brand
description: Launching a new brand
thumbnail: /assets/img/projects/launching-a-new-brand-cover.jpg
height: 720
width: 1280
categories: [Strategy]
brand: EJ Gallo
home: True
order: 3
sliderImages: ['/assets/img/projects/launching-a-new-brand-slider-1.jpg']
video: hgFc6SrZjVA
featureText: A new site and more authentic brand experiences
services: [Strategy, Branding, Design, Video Production, Measurement & Optimization, Interactive/Web]

description1: <h4>CHALLENGE</h4> <br /> <p>EJ Gallo launched an exciting new liqueur (Viniq), but sales the first two years were stagnant. Customers loved it, but brand awareness was low (3%) and consumer repeat rate was below within category. Viniq needed to solidify the brand experience. Website and content needed to answer the who, what and how questions about Viniq and support awareness through social media and improve engagement, repeat visits and brand affinity. </p>

featureData: ['10X <small>web traffic</small>', '3X<small>SALES growth YOY</small>', '1375%  <small>Higher Online Engagement</small>']

description2: <h4>IDEAS</h4> <p>desmond garcia knows Millennials and Gen Y, both current and future targets. We recommended that at zero moment of truth, the site must show and tell. Our target audience loves authentic experiences, social media and wants to know about new product offerings and different ways to use the product. So the .com experience was designed to be clean, mobile-first, and stacked with content that audiences are excited to pin, tweet, post and share with the world. We also created authentic brand experiences by creating a built-in microsite where users could post their own user generated content, and created social media calendars and posts that tied into this without major spending through the traditional social media channels. </p> <h4>RESULTS</h4> <p>After a many months of design, development and activation-- traffic, engagement and most importantly, sales increased 3X. Gallo created an additional SKU for the brand, resulting in even greater sales and additional content. This was a full service agency effort, from strategy, brand, design, video & photo production, social media management and other tasks.</p>

gallery: []
Source file: 
Notes: 
---
# My first blog post

Welcome to my first blog post using content module

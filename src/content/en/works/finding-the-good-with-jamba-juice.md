---
name: Finding the Good with Jamba Juice
slug: finding-the-good-with-jamba-juice
description: Finding the Good with Jamba Juice
thumbnail: /assets/img/projects/finding-the-good-with-jamba-juice-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: Jamba Juice
home: false
sliderImages: ['/assets/img/projects/finding-the-good-with-jamba-juice-slider-1.jpg']
video: DYd1JKEhOuc
featureText: Authentic stories work better.
services: [Video Production, Design, Strategy]

description1: <p>Jamba Juice's campaigns are known to promote health, well-being, and a new nutrious endeavor and identity.</p>

featureData: []

description2: <p>We worked with the brand to produce authentic stories that promoted these values and their products.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

---
name: Creating Teasers to ignite conversations
slug: creating-teasers-to-ignite-conversations
description: Creating Teasers to ignite conversations
thumbnail: /assets/img/projects/creating-teasers-to-ignite-conversations-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: FICO
home: false
sliderImages: ['/assets/img/projects/creating-teasers-to-ignite-conversations-slider-1.jpg']
video: BMSNgExnTIo
featureText: Building affinity through video.
services: [Video Production, Design, Strategy]

description1: <p>Teasers & short videos are a great way to build brand affinity. We produced and filmed many of these for FICO to help their data scientists sell more easily.</p>

featureData: []

description2: <p>FICO sells really really really complex products. To market them, they need storytelling and marketing assets which are cool, interesting and easy to digest. We took these very complex products and turned them into stories that anyone from any type of position could understand in this series of videos.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

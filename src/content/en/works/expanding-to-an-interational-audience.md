---
name: Expanding to an interational audience
slug: expanding-to-an-interational-audience
description: Expanding to an interational audience
thumbnail: /assets/img/projects/expanding-to-an-interational-audience-cover.jpg
height: 1325
width: 2000
categories: [Strategy]
brand: Invisalign
home: True
order: 1
sliderImages: ['/assets/img/projects/expanding-to-an-interational-audience-slider-1.jpg']
featureText: Global Customer Experience Messaging
services: [Strategy, Branding, Design, Video Production]

description1: <h4>CHALLENGE</h4> <br /> <p>Align Technology, the global leader in invisible orthodontics solution wanted to improve their relationship with North American, European, and Asian customers to deliver differentiated, exceptional, and consistent experiences. Align wanted to improve their responsiveness to the needs of their customers, increase loyalty, and lift the Net Promoter Score, implement customer experience improvements, as well as commit to changes in corporate culture and company policy.</p>

featureData: ['NEW VISION <small>for the future of CX<small>', 'GLOBAL <small>brand development<small>', 'SALES TEAMS <small>have tools to close deals<small>']

description2: <h4>IDEAS</h4> <p>desmond garcia executed a Global Customer Experience Messaging for North America, Europe & Asian markets which helped the company connect better with its customers. In addition to this broad strategic effort, desmondgarcia has created an iPad application for sales enablement for the North American Sales team and additional video content.</p> <h4>RESULTS</h4> <p>This engagement, which began as an analytic, strategic, and creative endeavor has resulted in strengthening Align Technology’s ability to do business and empowering their sales teams.</p>

gallery: []
Source file: 
Notes: 
---

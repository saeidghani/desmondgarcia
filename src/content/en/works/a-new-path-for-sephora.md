---
name: A new path for Sephora
slug: a-new-path-for-sephora
description: A new path for Sephora
thumbnail: /assets/img/projects/a-new-path-for-sephora-cover.jpg
height: 800
width: 800
categories: [Strategy]
brand: Sephora
home: true
sliderImages: ['/assets/img/projects/a-new-path-for-sephora-slider-1.jpg']
featureText: A deep dive within Sephora’s digital ecosystem
services: [Strategy, Branding , Design]

description1: <h4>CHALLENGE</h4><p>desmond garcia was engaged by Sephora to perform a complete digital audit that would provide technical insights for Sephora’s marketing leadership and content strategists to make directional decisions and improvements and “what’s next” for Sephora. The request was to complete this in an unbiased fashion, largely without sales and business operation context. Sephora has a ton of brands it represents in-store and online. Content, brand, messaging and “what do we have?” can become convoluted and forgotten in such a fast-paced environment.</p>

featureData: ['IMMEDIATE 4% <small>quarterly revenue growth<small>', 'DOUBLE DIGIT <small>YOY growth<small>', 'DEFINED SEPHORA <small>as a “digital disruptor”<small>']

description2: <h4>IDEAS</h4><p>desmond garcia did a deep dive within Sephora’s digital ecosystem, providing quantitative insights around “Where is everything & how is it performing” based off of all content category types in all digital platforms, from web, to social, to mobile and specialty applications. Next, we made specific suggestions for not only each channel, but also each media outlet/platform. These recommendations were based on existing consumer trends and future trends, like engagement via video, microsites, Snapchat and other platforms which may have been underutilized but certainly are effective and on the rise.</p><h4>RESULTS</h4><p>Sephora saw a 4% QOQ rise in revenue. That’s where it counts the most. Leveraging our discoveries and recommendations also resulted in an exponential drive in traffic through the site, more time spent per user on the site, platforms, better and deeper mobile engagement, and a more comprehensive brand story that works across the entire company.</p>

gallery: [/assets/img/projects/a-new-path-for-sephora-gallery-1.jpg, /assets/img/projects/a-new-path-for-sephora-gallery-2.jpg]
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Sephora Case Study In Deck
---

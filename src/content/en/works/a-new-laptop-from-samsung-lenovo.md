---
name: A new laptop from Samsung & Lenovo
slug: a-new-laptop-from-samsung-lenovo
description: A new laptop from Samsung & Lenovo
thumbnail: /assets/img/projects/a-new-laptop-from-samsung-lenovo-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: Samsung/Lenovo
home: false
sliderImages: ['/assets/img/projects/a-new-laptop-from-samsung-lenovo-slider-1.jpg']
video: LOBKLQioicE
featureText: A HQ video that tells a great story.
services: [Video Production, Design, Strategy]

description1: <p>When Lenovo was launching a new laptop in collaboration with Samsung, they reached out to us to help them tell a better story, design 3D animations of the product interiors and produce a high quality video that helped tell this story.</p>

featureData: []

description2:

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

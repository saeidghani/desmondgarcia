---
name: Maximizing YouTube visits
slug: maximizing-youtube-visits
description: Maximizing YouTube visits
thumbnail: /assets/img/projects/maximizing-youtube-visits-cover.jpg
height: 720
width: 1280
categories: [Measurement & Optimization]
brand: DocuSign 
home: false
sliderImages: ['/assets/img/projects/maximizing-youtube-visits-slider-1.jpg']
video: B3K1qJaEcJk
featureText: Millions of views, yes, millions
services: [Measurement & Optimization, Design, Strategy]

description1: <p>Want to know about how to make the best videos for YouTube preroll? Work with us! We worked with DocuSign to create dozens of videos produced, designed and edited for YouTube preroll.</p>

featureData: []

description2: <p>The result was millions, yes, millions of views conversions and strengthening the DocuSign brand. </p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: There's no photo asset for this as it's measurement & optimization
---
# My first blog post

Welcome to my first blog post using content module

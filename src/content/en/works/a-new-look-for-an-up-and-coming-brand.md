---
name: A new look for an up-and-coming brand
slug: a-new-look-for-an-up-and-coming-brand
description: A new look for an up-and-coming brand
thumbnail: /assets/img/projects/a-new-look-for-an-up-and-coming-brand-cover.jpg
height: 720
width: 1280
categories: [Interactive/Web]
brand: Castlight 
home: false
sliderImages: []
featureText: An extreme make over
services: [Interactive/Web, Measurement & Optimization, Design]

description1: <p>Castlight was in need of a brand refresh, from logo, to voice, and so we built and refreshed interactive experiences for them which reflected a new brand update and intention. The result, a super happy client, and increase revenues. </p>

featureData: []

description2: 

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use Castlight Castlight_Ebook_07.29.14 or Castlight_Ebook_Presentation02
---
# My first blog post

Welcome to my first blog post using content module

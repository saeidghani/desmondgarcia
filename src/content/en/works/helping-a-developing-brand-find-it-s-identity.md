---
name: Helping a developing brand find it's identity
slug: helping-a-developing-brand-find-it-s-identity
description: Helping a developing brand find it's identity
thumbnail: /assets/img/projects/helping-a-developing-brand-find-it-s-identity-cover.jpg
height: 720
width: 1280
categories: [Design]
brand: Castlight
home: false
sliderImages: ['/assets/img/projects/helping-a-developing-brand-find-it-s-identity-slider-1.jpg']
featureText: Brand new and improved assets
services: [Design, Strategy, Branding]

description1: <p>Castlight was in need of a brand refresh, new brochures, sales sheets, and outward facing marketing materials.</p>

featureData: []

description2: <p>We built and refreshed their materials which reflected the new brand, its awesome products, and their capabilities, all of which took this fledgling company to soaring new heights. </p>

gallery: []
Source file: https://drive.google.com/file/d/1pbXhRUFsI60leHixu3G3Grd8FRvkYDcA/view 
Notes: <li>Use Castlight_Health_One_Sheet_RGB.pdf</li>
---
# My first blog post

Welcome to my first blog post using content module

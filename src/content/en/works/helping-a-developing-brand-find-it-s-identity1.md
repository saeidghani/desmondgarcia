---
name: Helping a developing brand find it's identity
slug: helping-a-developing-brand-find-it-s-identity1
description: Helping a developing brand find it's identity1
thumbnail: /assets/img/projects/helping-a-developing-brand-find-it-s-identity1-cover.jpg
height: 720
width: 1280
categories: [Measurement & Optimization]
brand: Castlight
home: false
sliderImages: ['/assets/img/projects/helping-a-developing-brand-find-it-s-identity1-cover.jpg','/assets/img/projects/helping-a-developing-brand-find-it-s-identity1-slider-1.jpg']
featureText: Engaging more and more customers
services: [Measurement & Optimization, Design, Strategy]

description1: <p>Castlight knows results matter and was intent on understanding audience behavior, and then doing everything it could to optimize it.</p>

featureData: []

description2: <p>We assisted Castlight with analytics and understanding Google's findings and next steps to take to engage their customers for more time and turn early stage visitors into customers.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: There's no photo asset for this as it's measurement & optimization
---
# My first blog post

Welcome to my first blog post using content module

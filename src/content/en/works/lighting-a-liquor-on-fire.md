---
name: Lighting a liquor on fire
slug: lighting-a-liquor-on-fire
description: Lighting a liquor on fire
thumbnail: /assets/img/projects/lighting-a-liquor-on-fire-cover.jpg
height: 720
width: 1280
categories: [Design]
brand: EJ Gallo 
home: false
sliderImages: ['/assets/img/projects/lighting-a-liquor-on-fire-slider-1.jpg']
featureText: Lighting a liquor on fire
services: [Design, Strategy, Branding]

description1: <p>desmond garcia knows Millennials and Gen Y, both current and future targets. We recommended that at zero moment of truth, the site must show and tell. Our target audience loves authentic experiences, social media and wants to know about new product offerings and different ways to use the product. So the .com experience was designed to be clean, mobile-first, and stacked with content that audiences are excited to pin, tweet, post and share with the world. We also created authentic brand experiences by creating a built-in microsite where users could post their own user generated content, and created social media calendars and posts that tied into this without major spending through the traditional social media channels.</p>

featureData: []

description2: <p>After a many months of design, development and activation-- traffic, engagement and most importantly, sales increased 3X. Gallo created an additional SKU for the brand, resulting in even greater sales and additional content. This was a full service agency effort, from strategy, brand, design, video & photo production, social media management and other tasks.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: EJ Gallo Case Study
---
# My first blog post

Welcome to my first blog post using content module

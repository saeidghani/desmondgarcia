---
name: Animation for Mass Distribution
slug: animation-for-mass-distribution
description: Animation for Mass Distribution
thumbnail: /assets/img/projects/animation-for-mass-distribution-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: Rafter
home: false
sliderImages: ['/assets/img/projects/animation-for-mass-distribution-slider-1.jpg']
video: Vp2N5SzscCY
featureText: Animations that tell good stories.
services: [Video Production, Design, Strategy]

description1: <p>Rafter had a unique product and all the opportunity in the world. To universally promote this, they opted to create animations that would better tell the story to their audiences.</p>

featureData: []

description2: <p>Animations were simple and engaging ways to unite people over the product messaging, offering and everything else.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

---
name: Changing how Castlight does business
slug: changing-how-castlight-does-business
description: Changing how Castlight does business
thumbnail: /assets/img/projects/changing-how-castlight-does-business-cover.jpg
height: 800
width: 800
categories: [Branding]
brand: Castlight
home: false
sliderImages: []
featureText:
services: [Strategy, Branding, Design]

description1: <p>Due to the sensitive nature of branding we do not share brand work publicly. Here is a list of clients we've collaborated with on audience, personas, brand work, development & positioning.</p>

featureData: []

description2:

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Not sure what to do here. Open to suggestions
---
# My first blog post

Welcome to my first blog post using content module

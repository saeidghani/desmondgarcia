---
name: Growing a small brand to the global leader
slug: growing-a-small-brand-to-the-global-leader
description: Growing a small brand to the global leader
thumbnail: /assets/img/projects/growing-a-small-brand-to-the-global-leader-cover.jpg
height: 800
width: 800
categories: [Strategy]
brand: DocuSign
home: false
sliderImages: ['/assets/img/projects/growing-a-small-brand-to-the-global-leaders-slider-1.jpg']
video: XEDT8JjvAqE 
featureText: Creation of all types of content and design assets
services: [Strategy, Branding, Design, Video Production, Measurement & Optimization, Interactive/Web]

description1: <h4>CHALLENGE </h4> <br> <p>9 years ago we began working with a, then, small company called DocuSign. They were most known for helping people easily and securely sign, send, and manage documents in the cloud.  But the company was looking to move beyond just a simple signature application, to a much bigger offering of Digital Transaction Management, a platform that will help mid-market and enterprise-sized companies keep processes 100% digital from start to finish to accelerate transactions, reduce costs, and delight customers, partners, suppliers, and employees. What they needed was a way to explain their basic value proposition and growth, all the while attracting new enterprise and single user sales. </p>

featureData: ['$220M to $41.35B <small> company value </small>', '684% increase <small> in new users </small>', '100% ADOPTION <small> into Fortune 100  </small>' ]

description2: <h4>IDEAS </h4> <br> <p>desmond garcia brought DocuSign our core approach:overarching brand and content strategies, followed by design, video production, interactive assets and content creation. Since working with DocuSign, we’ve created broad content strategies, specific content strategies for campaigns, how-to videos, anthem videos, sizzle videos, product videos, messaging, micro-sites, and have supported DocuSign with the creation of all types of content and design assets for its major conferences, events and acquisitions</p> <br><br> <h4>RESULTS</h4> <br> <p>The results speak for themselves:684% increase in new users, adoption by all enterprise businesses in the Fortune 100, and an increase from $220M to $41.35B company value. </p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: DocuSign case study in deck
---
# My first blog post

Welcome to my first blog post using content module

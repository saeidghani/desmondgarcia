---
name: Using YouTube to earn small business
slug: using-you-tube-toearn-small-business
description: Using YouTube to earn small business
thumbnail: /assets/img/projects/using-you-tube-toearn-small-business-cover.jpg
height: 720
width: 1280
categories: [Video Production]
brand: DocuSign
home: True
sliderImages: ['/assets/img/projects/using-you-tube-toearn-small-business-slider-1.jpg']
video: 2sWqjaG_l5s
featureText: Effective and powerful video testimonials
services: [Video Production, Design, Strategy]
description1: <p>desmond garcia partnered with DocuSign to create a series of a dozen videos that were destined for the web, YouTube, social and more. These videos served different functions depending on their broadcast location, so we customized creative and strategic endeavors for each, including 1:1 ratio for mobile and customized graphics.</p>

featureData: ['Testimonials and peer recommendations are highly effective and powerful. 97% of B2B customers think these are the most reliable type of content']

description2: <p>This series of videos featured real customers, their businesses and their stories of business acceleration with DocuSign. desmond garcia led all production and creative efforts and this one, “THE ART OF FOOD” with Clay Williams has been talked about as one of the best and most effective videos of the year. Contact us to learn more about what we can do for you.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Can extract from Videos. How will this be designed?
---
# My first blog post

Welcome to my first blog post using content module

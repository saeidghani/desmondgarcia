---
name: Reaching a new global audience
slug: reaching-a-new-global-audience
description: Reaching a new global audience
thumbnail: /assets/img/projects/reaching-a-new-global-audience-cover.jpg
height: 720
width: 1280
categories: [Design]
brand: Invisalign 
home: false
sliderImages: ['/assets/img/projects/reaching-a-new-global-audience-slider-1.jpg']
featureText: A global customer experience
services: [Design, Strategy, Branding]

description1: <p>desmond garcia executed a Global Customer Experience Messaging for North America, Europe & Asian markets which helped the company connect better with its customers. In addition to this broad strategic effort, desmond garcia has created an iPad application for sales enablement for the North American Sales team and additional video content.</p>

featureData: []

description2: <p>This engagement, which began as an analytic, strategic, and creative endeavor has resulted in strengthening Align Technology’s ability to do business and empowering their sales teams.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use Invisalign CX Document
---
# My first blog post

Welcome to my first blog post using content module

---
name: An updated site for a global leader
slug: an-updated-site-for-a-global-leader
description: An updated site for a global leader
thumbnail: /assets/img/projects/an-updated-site-for-a-global-leader-cover.jpg
height: 720
width: 1280
categories: [Interactive/Web]
brand: Invisalign
home: false
sliderImages: ['/assets/img/projects/an-updated-site-for-a-global-leader-slider-1.jpg']
featureText: Upgrading to global standards
services: [Interactive/Web, Measurement & Optimization, Design]

description1: <p>desmond garcia executed a Global Customer Experience Messaging for North America, Europe & Asian markets which helped the company connect better with its customers.</p>

featureData: []

description2: <p>In addition to this broad strategic effort, desmond garcia has created an iPad application for sales enablement for the North American Sales team and additional video content.</p>

gallery: []
Source file: Case Study from page slide 10 of DG Capabilities Deck https://docs.google.com/presentation/d/1WtIcXe-WCcYue8sLHcsNKzX6XWrBOgm374Sh3DrV-RI/edit?usp=sharing
Notes: Use Invisalign Case Study
---
# My first blog post

Welcome to my first blog post using content module

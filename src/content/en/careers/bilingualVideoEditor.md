---
slug: bilingualVideoEditor
location: Remote
date: Apr 15 - 2021
category: Video
title: Bilingual Video Editor
summary: '
<p><strong>desmond garcia </strong>is an established strategic and marketing agency founded in 2019. Our personnel are currently located between Los Angeles and Miami, USA with a large base in Medellin, Colombia. Our primary in-house offerings include strategy, video production, web development, measurement & optimization, design, branding, and corporate philanthropy.</p>
<p>As we continue to grow internationally, we seek a talented Video Editor who can bring sight and sound together in order to tell a cohesive story on behalf of our clients.</p>
'

content: '
<h4>Responsibilities</h4>
<ul>
   <li>Manipulate and edit film pieces in a way that is invisible to the audience</li>
   <li>Take a brief to grasp production team’s needs and specifications</li>
   <li>Review shooting script and raw material to create a shot decision list based on scenes’ value and contribution to continuity</li>
   <li>Trim footage segments and put together the sequence of the film</li>
   <li>Input music, dialogues, graphics and effects</li>
   <li>Create rough and final cuts</li>
   <li>Ensure logical sequencing and smooth running</li>
   <li>Consult with stakeholders from production to post-production process</li>
   <li>Continuously discover and implement new editing technologies and industry’s best practices to maximize efficiency</li>
</ul>
<h4>Requirements</h4>
<ul>
   <li>Proven work experience as a Video Editor- 5 years</li> 
   <li>Bilingual 100%: English & Spanish </li>
   <li>Solid experience with digital technology and editing software packages (e.g. Avid Media Composer, Lightworks, Premiere, After Effects and Final Cut)</li>
   <li>Demonstrable video editing ability with a strong portfolio</li>
   <li>Thorough knowledge of timing, motivation and continuity</li>
   <li>Familiarity with special effects, 3D and compositing</li>
   <li>Creative mind and storytelling skills</li>
   <li>BS degree in film studies, cinematography or related field</li>
</ul>
<h4>About Desmond I Garcia</h4>
<p>desmond garcia is a creative agency that is grown quickly and continues to evolve. Our efforts span four pillars: content creation, storytelling, design, and experience, all of which manifest in the services that we offer.</p>
<p><strong>Our concept is simple</strong>, to help brands tell their story better across all kinds of content. We believe in leaving the stuffy bureaucracy behind and instead embracing simple honesty. We believe in working hard but also finding balance in what we do. Our policy is to do our best and show clients only our best work. Where we work, people trust and respect one another. We treat our clients like friends… because they are friends. No secrets, no office politics—no kidding! Essentially, our agency is passionate about dynamic ideas, powerful brand stories, and joyful working relationships. Can you see yourself in this space? If so, we want to hear from you. </p>
<p>Please send your CV in English and your Portfolio to <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com</a> </p>
<p>This is a full-time job. <br> Salary depending on experience. <br> Contract: prestación de servicios.</p>
'
---

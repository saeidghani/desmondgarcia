---
slug: bilingualVideoAnimator
location: Remote
date: Apr 15 - 2021
category: Video
title: Bilingual Video Animator
summary: '
<p><strong>desmond garcia </strong>is an established strategic and marketing agency founded in 2019. Our personnel are currently located between Los Angeles and Miami, USA with a large base in Medellin, Colombia. Our primary in-house offerings include strategy, video production, web development, measurement & optimization, design, branding, and corporate philanthropy.</p>
<p>As we continue to grow internationally, we seek a creative Video Animator to develop excellent visual frames with a range of techniques.</p>
<p>The ideal candidate will possess a creative eye and artistic aptitude. You should be able to easily turn a script or a concept into an impressive computer-generated animation. To excel in this role, you must be highly skilled in CGI software and storytelling.</p>
'

content: '
<p>We are always on the lookout for people who are eager to learn new things and enjoy being part of a talented team. It’s important that you’re committed to finding a way and making it happen in a dynamic, growing organization.</p>
<h4>Responsibilities</h4>
<ul>
   <li>Liaising with writers, directors, and clients regarding the style, method, and execution of the animation</li>
   <li>Reading scripts and storylines to understand animation requirements</li>
   <li>Preparing presentations with raw designs to customers</li>
   <li>Developing storyboards </li>
   <li>Preparing presentations of rough sketches for clients</li>
   <li>Creating models, drawings and illustrations</li>
   <li>Designing frames and characters</li>
   <li>Establishing artwork for the backgrounds and layouts</li>
   <li>Setting up exposure sheets, divided into actions and timing, dialogues and music, animation layers, backgrounds, view perspective</li>
   <li>Drawing rough sketches and animating </li>
   <li>Illustrating key frames and producing </li>
   <li>Inking, coloring, and clean ups</li>
   <li>Integrating images, characters, etc into background graphics</li>
   <li>Syncing visuals with audio and voiceover tracks</li>
   <li>Enhancing animation with special effects at post-production stage</li>
   <li>Using a range of animation software to achieve effective and impactful results </li>
</ul>
<h4>Skills in 2D Animators</h4>
<ul>
   <li>Proven experience as Video Animator</li>
   <li>Bilingual - English & Spanish </li>
   <li>Training in animation, both cel and computer graphics</li>
   <li>Ability to visualize abstract concepts</li>
   <li>Experience with storyboarding, clean ups, and layout design</li>
   <li>Drawing ability, artistic mindset</li>
   <li>Visual storytelling</li>
   <li>Ability to draw in a variety of styles and genres</li>
   <li>Strong interpersonal, communication and collaborative skills</li>
   <li>Knowledge of current techniques and trends in animation</li>
   <li>Ability to work under pressure and deliver on deadline</li>
   <li>Good communication skills</li>
   <li>Familiarity with a range of animation software and techniques</li>
   <li>Problem solving skills</li>
   <li>Ability to work to a schedule and plan workflows to meet deadlines</li>
</ul>
<h4>About Desmond I Garcia</h4>
<p>desmond garcia is a creative agency that is grown quickly and continues to evolve. Our efforts span four pillars: content creation, storytelling, design, and experience, all of which manifest in the services that we offer.</p>
<p><strong>Our concept is simple</strong>, to help brands tell their story better across all kinds of content. We believe in leaving the stuffy bureaucracy behind and instead embracing simple honesty. We believe in working hard but also finding balance in what we do. Our policy is to do our best and show clients only our best work. Where we work, people trust and respect one another. We treat our clients like friends… because they are friends. No secrets, no office politics—no kidding! Essentially, our agency is passionate about dynamic ideas, powerful brand stories, and joyful working relationships. Can you see yourself in this space? If so, we want to hear from you. </p>
<p>Please send your CV in English and your Portfolio to <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com</a> </p>
<p>This is a full-time job. <br> Salary depending on experience. <br> Contract: prestación de servicios.</p>
'
---




---
slug: partTimePersonalAssistant
location: Remote
date: Apr 15 - 2021
category: Business
title: Part Time Personal Assistant to the CEO
summary: '
<p>This is an exciting opportunity to work alongside the CEO of a quickly growing global company. The ideal candidate is a passionate and proactive person with excellent organizational skills and a hunger for career-advancement.</p>
<p>desmond garcia is an established strategic and marketing agency founded in 2019. Our personnel are currently located between Los Angeles and Miami, USA with a large base in Medellin, Colombia. Our primary in-house offerings include strategy, video production, web development, measurement & optimization, design, branding, and corporate philanthropy.</p>
'

content: '
<p>As we continue to grow internationally, we seek a personal assistant to provide personalized secretarial and administrative support to the CEO, President of LATAM, and COO. This is a part time job with pay of $2’000,000 COP per month and a contract prestacion de servicios. </p>

<h4>Responsibilities</h4>
<ul>
<li>Act as a buffer between the CEO and all points of contact </li>
<li>Act as the point of contact between the CEO, his staff, and external contacts and clients. </li>
<li>Screen the majority of incoming requests and needs made to the CEO</li>
<li>Organize the calendar and the daily business & personal life of the CEO </li>
<li>Contribute to the calendar organization of the President of LATAM</li>
<li>Manage the diary, schedule meetings, and make travel arrangements when required</li>
<li>Create documents in Google Docs, Sheets & Slides and more. </li>
<li>Source office and home supplies</li>
<li>Produce reports, presentations and briefs</li>
<li>Manage personal needs, e.g., housing, housing staff, repairs, purchases</li>
<li>Create basic operating budgets</li>
<li>Get bids for services and perform cost analysis to secure best value</li>
</ul>

<h4>Requirements</h4>
<ul>
<li>Medellin native with great local knowledge</li>
<li>Knowledge of well-known businesses</li>
<li>Great grammar, spelling, and punctuation in English</li>
<li>Meticulous attention to detail, able to deliver perfection in communication with high net worth clients.</li>
<li>Punctual, aggressive, and protective Type A personality</li>
<li>Proven work experience as a personal assistant</li>
<li>Team player </li>
<li>Fully bilingual in English and Spanish</li>
<li>Knowledge of office management systems and procedures</li>
<li>Knowledge and experience of using Google Apps for work or a commitment to quickly learn</li>
<li>Outstanding organizational and time management skills</li>
<li>Up to date with latest office gadgets and applications</li>
<li>Ability to multitask and prioritize daily workload</li>
<li>Excellent verbal and written communications skills</li>
<li>Discretion and confidentiality</li>
<li>Diploma or certification in business or business administration</li>
<li>Interest in marketing, design, video production or technology</li>
<li>Commitment to personal and professional development and ready to learn</li>
</ul>

<h4>About Desmond I Garcia</h4>
<p>desmond garcia is a creative agency that is grown quickly and continues to evolve. Our efforts span four pillars: content creation, storytelling, design, and experience, all of which manifest in the services that we offer.</p>
<p>Video Production (And Strategy) • Strategy • Social • Content Strategy and Execution (Video, Whitepapers, etc) • Interactive/ Web Development • Marketing Automation • Research • Brand Storytelling • Thought Leadership • Events • Design • Measure & Optimize (Brand & Digital Audits) • Philanthropy</p>

<p><strong>Our concept is simple</strong>, to help brands tell their story better across all kinds of content. We believe in leaving the stuffy bureaucracy behind and instead embracing simple honesty. We believe in working hard but also finding balance in what we do. Our policy is to do our best and show clients only our best work. Where we work, people trust and respect one another. We treat our clients like friends… because they are friends. No secrets, no office politics—no kidding! Essentially, our agency is passionate about dynamic ideas, powerful brand stories, and joyful working relationships. Can you see yourself in this space? If so, we want to hear from you.</p>

<p>Please send your resume in English to <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com</a> </p>
'
---




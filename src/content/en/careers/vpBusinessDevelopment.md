---
slug: vpBusinessDevelopment
location: Remote
date: Apr 15 - 2021
category: Business
title: VP of Business Development
summary: '
<p><strong>desmond garcia </strong>is an established strategic and marketing agency founded in 2019 with personnel working globally from Los Angeles, to Miami to a large base in Medellin. We are growing internationally and are looking for a Head of Business Development to grow our business with our existing United States client base, bring in additional projects, and generate new business with new clients in the $100MM to Fortune 20 range. If you’re an entrepreneur at heart, this is your chance to follow that voice. In this role, you will work with the following: </p>
<ul>
   <li>Our current clients to expand and generate new business from existing client contacts & their colleagues</li>
   <li>Previous clients. Reestablish connections with previous clients and grow business</li>
   <li>Our partnership agencies and PR companies. Create and exploit partnerships to grow the bottom line</li>
   <li>New target businesses in the $1B to Fortune 20 range</li>
</ul>
<p>You will sell desmond garcia services to help them accomplish their digital and strategic advertising, marketing and branding goals. </p>
<p>You will use your connections, our existing connections and your experience with Enterprise to Fortune companies to drive new business for our agency. You will work closely with desmond garcia leadership, partner agencies and existing client contacts, leverage existing contacts in new target businesses that map to our target industries, and build better relationships to accomplish financial quarterly objectives of revenue generation. You will anticipate how decisions are made, understand the details of individual campaigns and persistently explore and uncover the business needs of the key existing and potential clients. You will set the vision and the strategy for their advertising, marketing & brand goals and become a trusted advisor to our clients and partners. Your entrepreneurial drive, leadership, experience, understanding of the advertising, marketing and brand landscape will help set the forward trajectory for our expanding creative agency. You will work closely with the CEO, Head of Marketing, Senior Designers, and Head of Strategy and Project Managers to outline and execute revenue objectives. </p>
'
content: '
<h4>Responsibilities</h4>
<ul>
   <li>Consistently deliver personal and company monthly, quarterly and yearly revenue sales objectives.</li> 
   <li>Share and collaborate with the VP of Business Development CFO, CEO, vertical leadership and team members on new development through direct exposure to our clients and partners.</li> 
   <li>Develop and expand existing contacts to access and convert new business targets.</li>
   <li>Develop creative ways to drive agency development, knowledge of desmond garcia services, products and implementation of plans across the digital landscape.</li> 
   <li>Collaborate with cross-functional teams and departments (i.e. Finance, Creative, Design, and Video Production) for the fulfillment and execution of campaign tactics, deliverables and deadlines.</li> 
   <li>Development and preparation of client decks, recap reports and auditing. Competitive market research, analysis and recommendations on various business development opportunities.</li> 
   <li>General administrative support to agency accounting team and staff.</li> 
   <li>Generate new business and assist with closing.</li> 
   <li>Active prospecting and closing.</li> 
   <li>Identify and cultivate client relationships.</li> 
   <li>Evangelize the brand and prospect potential clients, CMOs, Directors & Executives.</li> 
   <li>Securing new strategic, creative and technical projects in the amount of 50.000 USD to $2,000,000 USD.</li> 
   <li>Providing the agency with $2,000,000 USD of new business during the calendar year.</li> 
   <li>Key Interfaces</li> 
   <li>The VP of Business Development will work closely with the staff members across all internal departments including Finance, Video Production, Brand, Design & Creative.</li> 
   <li>The VP of Business Development will report to the CEO.</li> 
   <li>This position will be based in our Medellin office location.</li> 
</ul>
<h4>Minimum Qualifications</h4>
<ul>
   <li>BA/BS degree or equivalent practical experience. </li>
   <li>10 years of experience in strategy, advertising sales, marketing, consulting, media and/or working with advertising or marketing agencies. </li>
   <li>Bilingual and English Speaking </li>
</ul>
<h4>Preferred Qualifications</h4>
<ul>
   <li>5+ years of sales &/or marketing experience, preferably within ad agencies or digital media. Startup experience is a big plus. </li>
   <li>Proven sales development success in revenue growth and meeting/exceeding annual sales quotas. </li>
   <li>Vast contact list and the ability to sell skill our offerings into target companies and industries including Fortune 500 brands. </li>
   <li>Proven track record for developing strong client relationships, and ability to work cross functionally. </li>
   <li>Strategic and analytical sales approach to building partnerships with advertisers and brands. </li>
   <li>Comfort with working on a dynamic and creative group. </li>
   <li>High level of written, verbal and presentation communication skills. </li>
   <li>Understanding of digital media, marketing, branding and advertising. </li>
   <li>Self-starter with strong communication skills as well as relationship and negotiation skills. </li>
   <li>100% rock-star self-managing performer. </li>
   <li>Ability to thrive in a growing start-up environment. </li>
   <li>Proven ability to manage multiple projects at a time while paying strict attention to detail. </li>
   <li>Proactive, independent worker with the demonstrated capacity to lead, motivate and work well with others. </li>
   <li>Independent, self-starter who thrives on immersion in a rapidly changing environment. </li>
</ul>
<h4>About Desmond I Garcia</h4>
<p>Is a creative agency that is quickly grown. Our efforts are based in four pillars,Content Creation, Storytelling, Design, and Experience, all which manifest in our following offerings:<br> • Qualitative audience research • Brand positioning and planning • Brand story & voice development • Brand messaging development • Content strategy • Content development • User experience design • Visual design • Integrated campaigns • Video strategy • Video production </p>
<p><strong>Our concept is simple</strong>, Help brands tell their story better in all forms of content. We believe in leaving bureaucracy behind and simply being honest, come what may. We believe in working hard, but also finding balance. We show clients only our best work. And our work environment is a place where people trust and respect one another and treat our clients like friends...because they are. No secrets, no office politics—no kidding! Simply put, our agency passionately brings dynamic ideas, powerful brand stories, and joyful working relationships to our in-house employees and to our customers. Can you see yourself here? If so, we want to hear from you. </p>
<p>Please send your cover letter and resume in English to <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com</a> </p>
<p>Contract Consultant + Commission <br> Salary depending on experience. <br> The Commission also offered.</p>
'
---




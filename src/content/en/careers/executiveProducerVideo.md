---
slug: executiveProducerVideo
location: Remote
date: Apr 15 - 2021
category: Video
title: Executive Producer - Video
summary: '
<p><strong>desmond garcia </strong>is an established strategic and marketing agency founded in 2019. Our personnel are currently located between Los Angeles and Miami, USA with a large base in Medellin, Colombia.</p>
<p>Our primary in-house offerings include strategy, video production, interactive/ web development, measurement & optimization, design, branding, and corporate philanthropy. As we continue to grow internationally, we seek an Executive Producer | Video, to lead, manage, and grow our video production unit. </p>
<p>This position requires considerable client-facing experience and the ability to perform and produce all video production tasks with excellence at every stage of the process, from client development to creative concepting, to scripting, pre-production, postproduction, and finishing.</p>
'

content: '
<p>You will be responsible for all business development activities related to video production including managing a P&L and providing strategic oversight on video projects with companies across a broad range of industries. The Executive Producer must have considerable experience in a fast-paced, dynamic environment and be able to demonstrate a history of new client and revenue acquisition. </p>
<p>You will work closely with desmond garcia executives, US and Colombia based clients, creative leadership, partner agencies and more. You will initiate, facilitate, bring to fruition and manage video projects. You will be responsible for generating revenue for the video vertical from existing and new clients and working laterally with VPs of Business Development, President LATAM, the CEO, and your direct reports.</p>
<p>You will anticipate how decisions are made, and you will research the details of our client’s brand, objectives, video projects and campaigns to a level of deep understanding. You will persistently explore, uncover, and solve the business needs of projects. You will set the vision for the video vertical from briefing through to launch and distribution.</p>
<p>Your entrepreneurial drive, leadership and experience will help set the forward trajectory for our expanding creative agency. </p>
<p>You will invest your work with an understanding of the advertising, marketing and brand landscape.</p>
<p>You will work closely with the Chief Strategy Officer, Business Development team, Project Manager and report directly to the CEO and President LATAM.</p>
<p>You will use your contacts to help us gain traction in the video production space in Colombia so our agency can evolve into highly competitive production studio.</p>

<h4>Responsibilities</h4>
<ul>
<li>Collaborate with clients to perform brand and video discovery</li>
<li>Determine concepts and goals of videos, write video treatments and scripts, manage production budgets and schedules</li>
<li>Lead the internal project team and oversee videographers, editors, motion graphic artists, and animators to manage every aspect of a video project from start to finish</li>
<li>Sell video production and postproduction services to new and existing clients, winning new contracts for video work</li>
<li>Manage the video department’s P&L and create & adjust businesses development strategies</li>
<li>Bring video projects to completion on time and on budget for desmond garcia clients, with a focus on optimizing efficiency and profit margin</li>
<li>Assure that projects achieve desired creative, business, and technical objectives</li>
<li>Evangelize the brand to clients, including C-Suite, Executives and Marketing Directors</li>
<li>Communicate, define, and manage the scope of the project to ensure client needs are met and budgets are maintained</li>
<li>Create and oversee project schedules including dependencies, duration and resources needed</li>
<li>Create and oversee internal and external expectations for project success and monitor quality standards for all project deliverables</li>
</ul>

<h4>Requirements</h4>
<ul>
<li>Minimum 5  years of executive producer experience in a digital agency or consulting firm working on large projects with 5 to 10 team members and diverse stakeholders</li>
<li>Must be Bilingual: English and Spanish </li>
<li>Digital agency experience preferred; digital project experience a must</li>
<li>Experience in bidding and acquiring work for companies in the $2BB to Fortune 20 range</li>
<li>xceptional attention to detail and follow-through </li>
<li>Experience utilizing various project management tools and software</li>
<li>Excellent communication, organizational, interpersonal, and writing skills</li>
<li>Excellent customer service and a pleasant approach under pressure</li>
<li>Proven ability to multi-task and interact with a large number of customers and cross-functional teams on a wide range of projects</li>
<li>Multi-platform campaign experience</li>
<li>Background in video production on the executive or sales level</li>
<li>Comfortable working with cross-functional teams including Brand, Strategy, User Experience, Creative, Video and Technology</li>
<li>Must possess a fundamental understanding of brand strategy, video production, mobile, and emerging technology</li>
</ul>

<h4>About Desmond I Garcia</h4>
<p>desmond garcia is a creative agency that is grown quickly and continues to evolve. Our efforts span four pillars: content creation, storytelling, design, and experience, all of which manifest in the services that we offer.</p>
<p><strong>Our concept is simple</strong>, to help brands tell their story better across all kinds of content. We believe in leaving the stuffy bureaucracy behind and instead embracing simple honesty. We believe in working hard but also finding balance in what we do. Our policy is to do our best and show clients only our best work. Where we work, people trust and respect one another. We treat our clients like friends… because they are friends. No secrets, no office politics—no kidding! Essentially, our agency is passionate about dynamic ideas, powerful brand stories, and joyful working relationships. Can you see yourself in this space? If so, we want to hear from you.</p>
<p>Please send your cover letter and resume in English to <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com</a> </p>
<p>This is a full-time executive position which is contract-to-hire over 60-90 days. Initial salary is commensurate with experience. <br> Contract prestación de servicios.</p>
'
---




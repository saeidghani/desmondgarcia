---
slug: midLevelGraphicDesigner
location: Remote
date: Mar 3 - 2021
category: Design
title: Mid level Graphic Designer
summary: '
<p><strong>desmond garcia </strong> is an established strategic and marketing agency founded in 2019. Our personnel are currently located between Los Angeles and Miami, USA with a large base in Medellin, Colombia. </p>
<p>Our primary in-house offerings include strategy, video production, interactive/ web development, measurement & optimization, design, branding, and corporate philanthropy. As we continue to grow internationally, we seek a committed, motivated Graphic Designer with Ui & Ux experience to join our talented global team. </p>
'
content: '
<p>This is the perfect opportunity for those who thrive in a dynamic work environment and strive to make it happen. You should know how to harness your creative flair to capture attention and communicate clearly using visual language. A strong ability to translate a message into design is a must. </p>
<p>If you can communicate well and work methodically as part of a team, we’d like to meet you. Our goal is always to inspire, attract, and engage the target audience.</p>

<h4>Requirements and qualifications</h4>
<ul>
<li>Graphic designer with Degree in Design, Fine Arts or related field and 4+ years of experience.</li>
<li>Demonstrated experience in graphic creation, branding, content creation, web design, Ui design, infographics design, and mockups.</li>
<li>Strong knowledge of Photoshop, Illustrator, InDesign, Adobe XD, Invision, Figma, and Sketch.</li>
<li>Able to test graphics across various media and amend designs according to feedback.</li>
<li>Experience in marketing campaign projects is a big plus.</li>
<li>UI/UX knowledge and experience is also highly desirable.</li>
<li>Knowledge of HTML, CSS, Less, Sass would be ideal.</li>
</ul>

<h4>Your responsibilities</h4>
<ul>
<li>Conceptualizing visuals based on briefs and determining requirements.</li>
<li>Creating interactive design that drives our marketing lead generation and product awareness.</li>
<li>Working side by side with our PMs and front-end developers to create design solutions that have a high visual impact for our marketing campaigns.</li>
<li>Communicating with clients and development team. </li>
<li>Researching interactive design trends.</li>
<li>General responsibilities related to the conceptualization and visual communication of brands and campaigns.</li>
</ul>

<h4>Soft Skills</h4>
<ul>
<li>Great interpersonal and communication skills</li>
<li>The ability to present ideas effectively</li>
<li>Intermediate to advanced English level</li>
<li>A keen eye for aesthetics and details</li>
<li>Ability to work methodically and meet deadlines</li>
</ul>

<p>You will work closely with the Product Designer UX/UI, the Business Development team, PR teams, designers, developers, video producers, and project managers.</p>

<h4>About Desmond I Garcia:</h4>
<p>desmond garcia is a creative agency that is grown quickly and continues to evolve. Our efforts span four pillars: content creation, storytelling, design, and experience, all of which manifest in the services that we offer.</p>
<p>Video Production (And Strategy) • Strategy • Social • Content Strategy and Execution (Video, Whitepapers, etc) • Interactive/ Web Development • Marketing Automation • Research • Brand Storytelling • Thought Leadership • Events • Design • Measure & Optimize (Brand & Digital Audits) • Philanthropy </p>
<p>Our concept is simple: to help brands tell their story better across all kinds of content. We believe in leaving the stuffy bureaucracy behind and instead embracing simple honesty. We believe in working hard but also finding balance in what we do. Our policy is to do our best and show clients only our best work. Where we work, people trust and respect one another. We treat our clients like friends… because they are friends. No secrets, no office politics—no kidding! Essentially, our agency is passionate about dynamic ideas, powerful brand stories, and joyful working relationships. Can you see yourself in this space? If so, we want to hear from you.</p>

<p>Please send your resume and portfolio in English to <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com </a> </p>
<p>This is a freelance position. <br>
Contract: Prestación de Servicios<br>
Initial salary negotiable based on experience.
</p>'
---

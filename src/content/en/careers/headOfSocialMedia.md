---
slug: headOfSocialMedia
location: Remote
date: Apr 15 - 2021
category: Social Media
title: Head of Social Media
summary: '
<p>desmond garcia is an established strategic and marketing agency founded in 2019. Our personnel are currently located between Los Angeles and Miami, USA with a large base in Medellin, Colombia. Our primary in-house offerings include strategy, video production, web development, measurement & optimization, design, branding, and corporate philanthropy.</p>

<p>Our clients are some of the world’s top brands and Fortune & Dinero 500 companies; for example, Google, Wells Fargo, Unilever, Visa, Sephora, DocuSign, Visa, Heineken, GSK, Comcast NBCU, and Nestle.</p>
'

content: '
<p>As we continue to grow internationally, we seek a Head of Social Media who will provide services to large Silicon Valley and Colombia-based companies. You will especially work with cause-driven social platforms similar to Wayfarer Studios and the Man Enough podcast.</p>
<p>The ideal candidate will be a combination of a business leader, visionary, inventor, builder, and operator. They must be passionate about the opportunity to work for desmond garcia and by the prospect of achieving success through social media on a global scale.</p>

<h4>Responsibilities</h4>
<ul>
<li>Developing a strategic social media marketing plan and managing its budget across campaigns and channels</<li>
<li>Working directly and indirectly with internal teams, agencies, publishers, platforms, and other partners to develop and execute creative ideas, plans, and strategies.</<li>
<li>Finding fast, creative, and efficient ways to build, execute, and measure innovative social media marketing plans that connect with customers and maximize engagement and impact</<li>
<li>Leading the development and direct the implementation of a wide range of content forms including video, text, still imagery, animation, and live-action</<li>
<li>Owning content development relationships with Facebook, Twitter, YouTube, Instagram, Snapchat, and other primaries/key partnership</<li>
<li>Developing partnerships and productive working relationships with program development teams, talent, publicists, showrunners, and other related stakeholders to build and leverage networking opportunities</<li>
<li>Providing inspirational and creative and thought leadership on ways to reach and gain the attention of target audiences</<li>
<li>Planning and managing campaign executions, on time, on budget, to scope</<li>
<li>Working closely with the team to integrate and measure the value and impact of social media marketing opportunities</<li>
<li>Managing external resources as necessary for that which we cannot do internally</<li>
<li>Establishing repeatable operational methodologies and mechanisms that enable frugal, fast, and responsive content development and distribution.</<li>
<li>Building, leading and managing a team of talented professionals.</<li>
</ul>

<h4>Qualifications</h4>
<ul>
<li>Bilingual in English & Spanish</li>
<li>BS in relevant discipline</li>
<li>10+ years in social media marketing and creative development with case studies to evidence experience</li>
<li>10+ years of management experience.</li>
<li>Experience leading small and large organizations (10 – 100+ people).</li>
<li>Experience working with big brands and in top agencies </li>
<li>Strong communication skills, both verbal and written</li>
</ul>

<h4>About Desmond I Garcia</h4>
<p>desmond garcia is a creative agency that is grown quickly and continues to evolve. Our efforts span four pillars: content creation, storytelling, design, and experience, all of which manifest in the services that we offer.</p>
<p>Video Production (And Strategy) • Strategy • Social • Content Strategy and Execution (Video, Whitepapers, etc) • Interactive/ Web Development • Marketing Automation • Research • Brand Storytelling • Thought Leadership • Events • Design • Measure & Optimize (Brand & Digital Audits) • Philanthropy</p>

<p>Our concept is simple: to help brands tell their story better across all kinds of content. We believe in leaving the stuffy bureaucracy behind and instead embracing simple honesty. We believe in working hard but also finding balance in what we do. Our policy is to do our best and show clients only our best work. Where we work, people trust and respect one another. We treat our clients like friends… because they are friends. No secrets, no office politics—no kidding! Essentially, our agency is passionate about dynamic ideas, powerful brand stories, and joyful working relationships. Can you see yourself in this space? If so, we want to hear from you.</p>

<p>If you meet the requirements please send your resume and cover letter in English to <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com</a> </p>
<p>Location: Remote (Medellin or Bogota preferred)</p>
'
---




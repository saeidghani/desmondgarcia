---
slug: seniorDigitalStrategist
location: Remote
date: Apr 5 - 2021
category: Marketing
title: Senior Digital Strategist  
summary: '
<p>desmond garcia is an established strategic and marketing agency founded in 2019. Our personnel are currently located between Los Angeles and Miami, USA with a large base in Medellin, Colombia. Our primary in-house offerings include strategy, video production, web development, measurement & optimization, design, branding, and corporate philanthropy.</p>
<p>Our clients are some of the world’s top brands and Fortune & Dinero 500 companies; for example, Google, Wells Fargo, Unilever, Visa, Sephora, DocuSign, Visa, Heineken, GSK, Comcast NBCU, and Nestle.</p>
<p>As we continue to grow internationally, we seek a Senior Digital Strategist to support and lead a variety of client-facing brand and strategic efforts. </p>
'
content: '
<p>The Senior Digital Strategist is responsible for managing multiple projects both internally and with clients. You will be expected to effectively collaborate with developmental, creative, brand management, and digital innovation teams.</p>
<p>Success in this role demands strong strategic and brand capabilities alongside analytical orientation. Exceptional communication skills are required, as is a collaborative approach. Vision, energy, enthusiasm, and focus are important, but so is sensitivity. You will be expected to work effectively across a broad spectrum of people.</p>

<h4>Digital Strategy</h4>
<ul>
<li>Building digital marketing roadmaps for clients and for desmond garcia as a brand </li>
<li>Planning, implementing, and managing the overall digital marketing strategy for desmond garcia and lead client projects</li>
<li>Overseeing our social media accounts and those of clients when project scopes require it to ensure that social is continuously growing and that social content is forward thinking and engaging</li>
<li>Staying up-to-date with digital technology developments</li>
</ul>

<h4>Branding</h4>
<ul>
<li>Running research and strategy projects for clients including (but not limited to): competitive analysis, brand DNA creation, positioning pyramids, brand tone of voice/ verbal identity and brand book creation</li>
<li>Leading outwardly facing projects for high-level clients that require brand development, including voice, identity, positioning, vision and more</li>
<li>Developing all digital creative outputs inclusive of web promotional materials, selection of creative agencies, creative briefings, graphics, and photography across the business</li>
<li>Creating and placing advertising that is in support of the business’ and sales team’s initiatives through media planning, creative development, metrics, market research, and vendor management</li>
<li>Creating all forms of marketing including interactive marketing, direct marketing campaigns, advertising, sales collateral, press releases, and executive presentations</li>
<li>Providing inspiration and direction for the teams in regular creative sessions.</li>
<li>Influencing brand direction at a senior level with ideas, inspiration, and creative thinking</li>
<li>Ensuring the completion of market research analyses and monitoring competitive activity in the market as well as conducting gap analyses and financial modeling</li>
<li>Working with brand campaign managers in ensuring relevant brand promotion that will lead to the achievement of maximum ROI</li>
<li>Creating & executing successful brand campaigns</li>
<li>Working with the Director on unique, ground-breaking projects
<li>Guiding graphic designers to ensure content is on brand and on time</li>
<li>Building brand deck and guidelines to drive consistency through the business</li>
</ul>

<h4>General</h4>
<ul>
<li>Building relationships with all ambassadors and key influencers</li>
<li>Bringing on board new collaborators</li>
<li>Playing a mentorship role to key personnel in the brand marketing department and honing their professional skills</li>
<li>Write regular reports for the business’s leadership, which must be easily digestible, unambiguous, engaging, informative and convincing</li>
</ul>

<h4>You should</h4>
<ul>
<li>Be highly motivated and organized</li>
<li>Have in-depth marketing experience and a passion for digital technologies</li>
<li>Be a good strategist who is able to develop plans and bring them to fruition</li>
<li>Be a strong multitasker who is able to lead and manage teams</li>
<li>Be extremely creative</li>
<li>Actively take an interest in social and cultural trends and developments in the digital sector </li>
<li>Be comfortable with the prospect of working directly with multiple clients and new business pitches at any given time</li>
</ul>

<h4>Skills and Experience</h4>
<ul>
<li>Have 10+ years of relevant advertising/digital marketing experience</li>
<li>Graduate degree in marketing, communications, advertising</li>
<li>Diploma or postgraduate degree in digital marketing </li>
<li>Experience of working and reporting to boards and committees</li>
<li>Significant experience of managing commercial and matrix relationships in a large/comparable organization</li>
<li>Experience leading, coaching and managing employees</li>
</ul>

<h4>Digital Experience</h4>
<ul>
<li>Experience in digital marketing and brand strategy is a must </li>
<li>Demonstrable experience in designing and implementing successful digital marketing campaigns</li>
<li>Strong understanding of how all current digital marketing channels function</li>
<li>Solid knowledge of online marketing tools and best practices</li>
<li>Hands on experience with SEO/SEM, Google Analytics and CRM software</li>
<li>Familiarity with web design - understanding of UX/UI and web analytics</li>
<li>Experience with content marketing </li>
<li>Experience with performance marketing </li>
<li>Experience with digital advertising </li>
</ul>

<h4>Branding Experience</h4>
<ul>
<li>A good record of achievement at a senior level in the field of brand strategy, especially in the digital sector or an industry of similar scale and complexity</li>
<li>Senior experience in directing a brand team to deliver unified messages to targeted audiences, based on the highest creative standards and sound creative judgment</li>
<li>Senior experience in managing global brands which manifest differently in different markets around the world</li>
<li>Experience in developing brand identities</li>
<li>A full understanding of audiences and ability to respond and anticipate in an ever changing media landscape</li>
<li>Proven ability to clearly convey information and instructions, which will determine the effectiveness with which strategies are executed </li>
<li>Evidence of success in a results-driven environment </li>
<li>Strong presentational and influencing skills </li>
<li>Track record of successfully delivering change </li>
<li>Experience of developing and implementing guidelines and working processes in a broad range of areas</li>
</ul>

<h4>About desmond garcia</h4>
<p>desmond garcia is a creative agency that is grown quickly and continues to evolve. Our efforts span four pillars: content creation, storytelling, design, and experience, all of which manifest in the services that we offer.</p>
<p>Video Production (And Strategy) • Strategy • Social • Content Strategy and Execution (Video, Whitepapers, etc) • Interactive/ Web Development • Marketing Automation • Research • Brand Storytelling • Thought Leadership • Events • Design • Measure & Optimize (Brand & Digital Audits) • Philanthropy</p>
<p>Our concept is simple: to help brands tell their story better across all kinds of content. We believe in leaving the stuffy bureaucracy behind and instead embracing simple honesty. We believe in working hard but also finding balance in what we do. Our policy is to do our best and show clients only our best work. Where we work, people trust and respect one another. We treat our clients like friends… because they are friends. No secrets, no office politics—no kidding! Essentially, our agency is passionate about dynamic ideas, powerful brand stories, and joyful working relationships. Can you see yourself in this space? If so, we want to hear from you. </p>
<p>Please send your resume in English to <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com</a> </p>
'
---




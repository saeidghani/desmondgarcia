---
slug: executiveCreativeDirector
location: Remote
date: Mar 3 - 2021
category: Design
title: Executive Creative Director
summary: '
<p><strong>desmond garcia </strong>is an established strategic and marketing agency founded in 2019. Our personnel are currently located between Los Angeles and Miami, USA with a large base in Medellin, Colombia. Our primary in-house offerings include strategy, video production, web development, measurement & optimization, design, branding, and corporate philanthropy. </p>
<p>As we continue to grow internationally, we seek an extraordinary Executive Creative Director who will be responsible for the overall supervision of the agency’s creative product. </p>
<p>The Executive Creative Director interfaces with the agency’s account team and clients; they mentor the creative department, and they are an integral part of the agency’s new business team. This is an important role. If you are a rockstar who’s ready to be the creative face of an exciting brand, consider this is your opportunity. </p>
'
content: '
<p>The position requires the proven conceptual ability to develop award-winning traditional and digital ad concepts. You must also be able to support and inspire the agency’s creative department to achieve at the same level of excellence.</p>
<h4>Responsibilities</h4>
<h5>Strategize</h5>
<ul>
<li>Develop, implement, and monitor the annual creative department plan to support the agency annual business plan and vision.</li><li>
Work with the CEO to develop a clearly articulated creative philosophy for the agency.</li><li>
<li>Oversee the ongoing development and refinement of creative sections of the agency’s new business documents.</li><li>
<li>Read and digest all creative work plans in a timely manner.</li><li>
<li>Regularly review the latest national award books and online creative resources to stay abreast of current creative trends and styles.</li><li>
<li>Deliver strategic communication solutions that help our clients attain their marketing and communications objectives.</li><li>
<li>Ask and listen to fully understand the client situation before translating it into strategic brand concepts, voice, messages, and rationales.</li><li>
</ul>
<h5>Represent</h5>
<ul>
<li>Act as art director on major accounts, leading the creative department by example.</li>
<li>Clearly articulate the agency’s creative philosophy to clients and represent the agency to the highest standards.</li>
<li>Reinforce the creative philosophy of the agency by regularly articulating it to staff and clients during internal and external presentations.</li>
<li>Give regular progress reports on how the agency is achieving its creative goals to the entire staff during regular meetings.</li>
<li>Meet regularly with key clients to build relationships, and review how our creative philosophy can add value to their marketing efforts.</li>
<li>Actively participate in the onboarding process, maximizing the agency’s chances of landing new clients.</li>
Oversee the creative department development of campaigns for new business as well as creative presentations, presenting our creative philosophy and strategy in these meetings.</li>
<li>Work with the agency’s PR partner to place stories of successful campaigns in the local, regional, and national press.</li>
</ul>
<h5>Mentor</h5>
<ul>
<li>Regularly update creative department staff on the latest trends in advertising and connect them to useful resources for their development, such as award show books.</li>
<li>Provide ongoing coaching to department employees on how to conceptualize, develop, and sell award-winning work.</li>
<li>Provide ongoing coaching to account staff on how they can support the creative department mission.</li>
<li>Ensure the professional development and readiness of the creative team.</li>
<li>Manage the evolution of the agency’s technology needs and marketing technology and digital marketing capabilities.</li>
<li>Co-lead the unified creative/account management process to execute on client projects.</li>
<li>Lead the unified process of marketing/concept/design and interactive/web/video/motion graphics.</li>
</ul>
<h5>Mentor</h5>
<ul>
<li>Oversee all creative department employees and the entire creative process, ensuring that clients consistently receive creative and quality service that meets their strategic goals.</li>
<li>Review all creative work at the concept stage and at every stage of the design and production process. </li>
<li>Ensure that all creative work is of comparable creative quality to ads found in the latest national advertising annuals and online creative showcase sites. </li>
<li>Work with creative staff to ensure that digital ideas are an integral part of any recommended campaign concepts.</li>
<li>Supervise photographers, illustrators, and other production vendors.</li>
<li>Ensure that all ad copy is proofread by department personnel before it is released to media vendors.</li>
<li>Meet all agreed-upon deadlines established by account service and when not possible, immediately negotiate a new deadline with the account service department. </li>
<li>Maximize the efficiency and profitability of the creative function.</li>
</ul>
<h4>Desired Qualities of the Creative Director</h4>
<p>You are brilliant. <br>You are turned on by technology, digital, and design trends, and you are fascinated by the continual evolution of the Internet and digital marketing. You are undaunted by the newest marketing technologies. You don’t wince when people say print is dying. You see opportunities.<br>You are a problem solver and an information architect. You’re able to work with senior business minds to help them articulate their message. Then you can translate that message into an identity using compelling creativity and breakthrough innovation.<br>You are an energizing person, not only with your work but with your work ethic and style. You can unite people in a team and invite them to share in co-ownership of ideas. <br>
You are proud of your work, but ultimately you realize that it belongs to the client and the customer. You are not cynical, you are not inflexible, and you are not about your portfolio. You are about being part of something as great as you are. </p>
<h4>Primary Duties</h4>
<ul>
<li>Work collaboratively with partners, participate in company strategizing, and make recommendations for changes to the creative function to meet market changes, competitive threats or to support strategic plans.</li>
<li>Understand desmond garcia’s strategic goals and brand; analyze the creative team, environment, processes, technologies, etc., to determine areas needing improvement and build and direct a creative function aligned with and supportive of those goals.</li>
<li>Make daily decisions that ensure both profitability and creative quality.</li>
<li>Set expectations and standards (build a culture) for creative team attitude, behavior, teamwork and professional development. Lead accordingly.</li>
<li>Direct the activities and professional development of creative team members, including conducting staff reviews, taking corrective actions, and recommending promotions.</li>
<li>Drive constant improvement in creative quality and capabilities.</li>
<li>With the account lead, assure that internal and client presentations are on time, professional, engaging, persuasive, and supported by a sound rationale.</li>
<li>Assure that timely and informative communication takes place between the creative function and the partners, account team, and clients.</li>
<li>Contribute to the strategic and creative development of the agency’s marketing and branding efforts for new business development.</li>
<li>Ensure self and staff development in web-based and interactive marketing capabilities.</li>
</ul>

<h4>Secondary Duties</h4>
<ul>
<li>Work collaboratively with partners, participate in company strategizing, and make recommendations for changes to the creative function to meet market changes, competitive threats or to support strategic plans.</li>
<li>Understand desmond garcia’s strategic goals and brand; analyze the creative team, environment, processes, technologies, etc., to determine areas needing improvement and build and direct a creative function aligned with and supportive of those goals.</li>
<li>Make daily decisions that ensure both profitability and creative quality.</li>
<li>Set expectations and standards (build a culture) for creative team attitude, behavior, teamwork and professional development. Lead accordingly.</li>
<li>Direct the activities and professional development of creative team members, including conducting staff reviews, taking corrective actions, and recommending promotions.</li>
<li>Drive constant improvement in creative quality and capabilities.</li>
<li>With the account lead, assure that internal and client presentations are on time, professional, engaging, persuasive, and supported by a sound rationale.</li>
<li>Assure that timely and informative communication takes place between the creative function and the partners, account team, and clients.</li>
<li>Contribute to the strategic and creative development of the agency’s marketing and branding efforts for new business development.</li>
<li>Ensure self and staff development in web-based and interactive marketing capabilities.</li>
</ul>

<h4>Functional Competencies</h4>
<ul>
<li>Superior leadership and collaboration skills</li>
<li>Superior problem solving and decision-making skills</li>
<li>Excellence in managing and motivating staff</li>
<li>Excellent communication and persuasion skills</li>
<li>Superior understanding of advertising and marketing principles</li>
<li>Superior understanding of marcom strategies and tactics, including technologies</li>
<li>Demonstrated talent for high-calibre creative concepting and writing</li>
<li>Ability to judge creative concepts and copy, & clearly communicate creative direction to others</li>
<li>Thorough understanding of clients and their business/industry</li>
<li>Excellent presentation skills</li>
<li>Thorough understanding of art direction</li>
<li>Excellent communication, planning, time-management and follow-through skills</li>
<li>Excellent knowledge of InDesign</li>
<li>Working knowledge of Google Apps for Work</li>
<li>Working knowledge of Flash, Photoshop and Illustrator</li>
<li>Understanding of interactive media production</li>
<li>Must be Bilingual (English & Spanish)</li>
</ul>

<h4>About Desmond I Garcia:</h4>
<p>desmond garcia is a creative agency that is grown quickly and continues to evolve. Our efforts span four pillars: content creation, storytelling, design, and experience, all of which manifest in the services that we offer. <br>Video Production (And Strategy) • Strategy • Social • Content Strategy and Execution (Video, Whitepapers, etc) • Interactive/ Web Development • Marketing Automation • Research • Brand Storytelling • Thought Leadership • Events • Design • Measure & Optimize (Brand & Digital Audits) • Philanthropy</p>
<p>Our concept is simple: to help brands tell their story better across all kinds of content. We believe in leaving the stuffy bureaucracy behind and instead embracing simple honesty. We believe in working hard but also finding balance in what we do. Our policy is to do our best and show clients only our best work. Where we work, people trust and respect one another. We treat our clients like friends… because they are friends. No secrets, no office politics—no kidding! Essentially, our agency is passionate about dynamic ideas, powerful brand stories, and joyful working relationships. Can you see yourself in this space? If so, we want to hear from you. </p>
<p>Please send your cover letter and resume to: <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com </a> </p>
<p>This is a full-time executive position which is contract-to-hire over 60-90 days. Initial salary is commensurate with experience.<br>Contract prestación de servicios.</p>
'
---

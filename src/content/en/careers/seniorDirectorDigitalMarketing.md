---
slug: seniorDirectorDigitalMarketing
location: Remote
date: Apr 5 - 2021
category: Marketing
title: Senior Director - Digital Marketing  
summary: '
<p><strong>desmond garcia</strong> is an established strategic and marketing agency founded in 2019. Our personnel are currently located between Los Angeles and Miami, USA with a large base in Medellin, Colombia. Our primary in-house offerings include strategy, video production, web development, measurement & optimization, design, branding, and corporate philanthropy. <br>Responsibilities of the Marketing Director will include managing the marketing budget and ensuring that all marketing material is in line with our brand identity as well as tracking and analyzing the performance of advertising campaigns. 
</p>
'
content: '
<p>To be successful in this role, you should have hands-on experience working with different channels including web, social media, PR, and analytics tools. You should be able to turn creative ideas into effective advertising projects that get results. You must also keep your team motivated and nurture each member’s creativity and innovation.<br> Ultimately, your role is to help us build and maintain a strong and consistent brand through a wide range of online and offline marketing channels. We are always on the lookout for people with potential who are eager to learn new things and enjoy being part of a talented team. desmond garcia is made up of committed, motivated individuals who make it happen alongside hard-working leaders in a dynamic, evolving organization.</p>

<h4>Responsibilities</h4>
<ul>
<li>Undertake daily administrative tasks to ensure the functionality and coordination of the department’s activities.</li>
<li>Support the creative director in the management of creative staff such as designers and copywriters. </li>
<li>Develop and implement marketing and content strategy.</li>
<li>Test (in market) best content approaches and how to apply new branding.</li>
<li>Identify optimal marketing channels to drive desired outcomes.</li>
<li>Determine the voice and story of the content and brand, then its application across channels.</li>
<li>Contribute to marketing desk as a team member.</li>
<li>Generate statistical reports derived from marketing research such as A/B testing. </li>
<li>Create creative marketing plans including content creation, audience engagement, growth metrics & management.</li>
<li>Measure and report on the performance of marketing campaigns to gain insight and assess against goals.</li>
<li>Employ marketing analytics techniques including web analytics and rankings to gather important data.</li>
<li>Assist in the organization of promotional events and traditional or digital campaigns.</li>
<li>Experiment with a variety of organic and paid acquisition channels like content creation, content curation, pay per click campaigns, event management, publicity, social media, lead generation campaigns, copywriting, and performance analysis.</li>
</ul>

<h4>Requirements</h4>
<ul>
<li>BSc in Marketing, Business, or relevant field</li>
<li>Proven experience as a Senior Marketing Manager</li>
<li>Proven experience in identifying target audiences and in creatively devising and leading engaging marketing campaigns across a range of channels</li>
<li>Good understanding of office management and marketing principles</li>
<li>Demonstrable ability to multi-task and adhere to deadlines</li>
<li>Well-organized and customer-oriented approach</li>
<li>Good knowledge of market research techniques and databases</li>
<li>Excellent knowledge of MS Office, marketing computer software and online applications (CRM tools, Online analytics, Google Adwords etc.)</li>
<li>Exquisite communication and people skills</li>
<li>A sense of aesthetics and a love for great copy and witty communication</li>
<li>Up to date with the latest trends and best practices in online marketing and measurement</li>
</ul>

<h4>About desmond  garcia:</h4>
<p>is a creative agency that is grown quickly and continues to evolve. Our efforts span four pillars: content creation, storytelling, design, and experience, all of which manifest in the services that we offer.</p>
<p>Video Production (And Strategy) • Strategy • Social • Content Strategy and Execution (Video, Whitepapers, etc) • Interactive/ Web Development • Marketing Automation • Research • Brand Storytelling • Thought Leadership • Events • Design • Measure & Optimize (Brand & Digital Audits) • Philanthropy</p>
<p><strong>Our concept is simple</strong>, to help brands tell their story better across all kinds of content. We believe in leaving the stuffy bureaucracy behind and instead embracing simple honesty. We believe in working hard but also finding balance in what we do. Our policy is to do our best and show clients only our best work. Where we work, people trust and respect one another. We treat our clients like friends… because they are friends. No secrets, no office politics—no kidding! Essentially, our agency is passionate about dynamic ideas, powerful brand stories, and joyful working relationships. Can you see yourself in this space? If so, we want to hear from you. </p>
<p>Please send your cover letter and resume in English to: <a href="mailto:nat@desmondgarcia.com">nat@desmondgarcia.com</a> </p>
<p>This is a full-time job. <br> Salary negotiable based on experience. <br> Contract: prestación de servicios.</p>
'
---




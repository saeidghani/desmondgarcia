// When the user scrolls the page, execute myFunction
window.onscroll = function () { myFunction() }

// Get the header
const header = document.getElementById('header')

// Get the offset position of the navbar
const sticky = header.offsetTop

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction () {
  if (window.pageYOffset - 25 > sticky) {
    header.classList.add('sticky')
  } else {
    header.classList.remove('sticky')
  }
}

function toggleMenu () {
  const element = document.getElementById('body')
  element.classList.toggle('openMenu')
}

// external js: isotope.pkgd.js

// init Isotope
var $grid = $('.works').isotope({
  itemSelector: '.work-box',
  layoutMode: 'masonry',
  percentPosition: false
})

// bind filter button click
$('.filters-button-group').on('click', 'button', function () {
  $('.filters-button-group button').removeClass('active')
  $(this).addClass('active')
  const filterValue = $(this).attr('data-filter')
  $grid.isotope({ filter: filterValue })
})

window.onload = function () {
  document.getElementById('body').classList.add('onload')
}

var $grid = $('.project-galley').isotope({
  itemSelector: '.galley-item',
  layoutMode: 'masonry',
  percentPosition: false
})

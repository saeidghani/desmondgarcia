import Vue from 'vue'
import scrollLoader from 'vue-scroll-loader'

Vue.use(scrollLoader, { /* options */ })
